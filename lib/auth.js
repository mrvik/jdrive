const fs=require("fs")
const readline=require("readline");
const {google}=require("googleapis");

/**
 * Authentication module
 * @module lib/auth
 */

/**
 * @typedef {Object} OAuth2
 */

/**Does all the work with google oauth */
class Auth{

    /**
     * Set the properties to the class
     * @param {Object} a - Options object
     * @param {Object} a.files - Contains config files
     * @param {string[]} a.scopes - Auth scopes
     * @param {string} a.token - Access token (if exists)
     * @param {Object} a.creds - Credentials for Oauth2
     */
    constructor(a){
        this.files=a.files;
        this.scopes=a.scopes;
        this.lock=this.init(a);
    }

    /**
     * Set oauth client to this.client
     * @async
     * @param {Object} a - Options object
     * @param {string} a.token - Access token (if exists)
     * @param {Object} a.creds - Credentials for Oauth2
     * @returns {Promise<undefined>} Fullfills when the Auth is successful
     */
    async init(a){
        const {creds,token}=a;
        this.client=await this.oauth(creds,token);
    }

    /**
     * Get oauth client
     * @async
     * @param {Object} creds - Oauth credentials
     * @param {string} token - Existing token
     * @returns {Promise<OAuth2>} Oauth2 client object
     */
    async oauth(creds,token){
        const {client_secret, client_id, redirect_uris} = creds.installed;
        const client=new google.auth.OAuth2(client_id,client_secret,redirect_uris[0]);
        if(!token){
            token=await this.getToken(client).then(a=>this.save(this.files.token,a));
        }
        client.setCredentials(token);
        return client;
    }

    /**
     * Get token using Oauth2 client and reading auth from user
     * @async
     * @param {OAuth2} client - Get authUrl from it
     * @returns {Promise<string>} Token
     */
    async getToken(client){
        const authUrl=client.generateAuthUrl({
            access_type: "offline",
            scope: this.scopes
        });
        console.log("Authorize this app visiting: ",authUrl);
        const rl=readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        const code=await new Promise(resolve=>{
            rl.question("Enter the generated code: ",c=>{
                resolve(c);
                rl.close();
            });
        });
        return await new Promise((resolve,reject)=>{
            client.getToken(code, (err,token)=>{
                if(err)return reject(err);
                return resolve(token);
            });
        });
    }

    /**
     * Write file to fs
     * @param {string} file - File path
     * @param {Object} cont - Contents of the file. Stringified before writing
     * @returns {Object} Contents written in the file (not stringified)
     */
    save(file,cont){
        fs.writeFileSync(file,JSON.stringify(cont),{
            mode: 0o600
        });
        return cont;
    }
}
module.exports={
    Auth
};
