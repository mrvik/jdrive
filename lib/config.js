/**
 * Manage the configuration for JDrive
 * @module lib/config
 */

const fs=require("fs-extra");
const path=require("path");

class Config{

    /**
     * @param {Object} a - Arguments to config
     * @param {Object} a.args - Parsed commandline arguments
     */
    constructor(a){
        const {args}=a;
        let base=args.config_dir||path.resolve(`${process.env.HOME}/.config/jdrive`);
        this.base=base;
        this.files={
            userConfig: this.getPath(args.config,"jdrive.json"),
            credentials: this.getPath(args.credentials,"credentials.json"),
            token: this.getPath(args.token,"token.json"),
            lock: this.getPath(args.lockfile,"lockfile.json"),
            ignore: this.getPath(args.ignore_file,".syncignore"),
            localDir: args.local_dir||`${process.env.HOME||"/tmp"}/Google Drive/` // This should be an absolute path
        };
        this.stopAfterAuth=args.auth_only;
        this.dryRun=args.dryRun;
        this.config=this.generateConfig();
        this.replaceFiles();
        this.getIgnored();
        this.lock=this.verifyDirs();
        global.JDriveID=args.id;
    }

    /**
     * Get path for a config file
     * @param {string} p - Provided path
     * @param {string} def - Default property value
     * @returns {string} An absolute file path
     */
    getPath(p=false,def){
        return path.resolve(p&&path.isAbsolute(p)?p:path.join(this.base,p?p:def)); //Use p if absolute, else join base and p if p is defined, else join with def
    }

    /**
     * Generates a config object from system wide and user configs
     * @returns {Object} Merged config
     */
    generateConfig(){
        const initial={};
        try{
            initial.global=JSON.parse(fs.readFileSync("/etc/jdrive.json"));
        }catch(e){
            console.error("No global config on /etc/jdrive.json");
        }
        try{
            initial.user=JSON.parse(fs.readFileSync(this.files.userConfig));
        }catch(e){
            console.error("No user config, falling back to defaults")
        }
        //eslint-disable-next-line prefer-object-spread
        return Object.assign({},initial.global,initial.user);
    }

    /**
     * Replace files with config ones
     * @returns {undefined} void
     */
    replaceFiles(){
        if(this.config.files){
            for(let i in this.files){
                if(!this.files.hasOwnProperty(i))continue;
                if(this.config.files[i])this.files[i]=this.config.files[i];
            }
        }
    }

    /**
     * Populate this.config.local with ignored files from this.files.ignore
     * @returns {undefined} void
     */
    getIgnored(){
        try{
            let f=fs.readFileSync(this.files.ignore,"UTF-8")
                .split("\n")
                .filter(l=>l!='');
            if(!this.config.local)this.config.local={};
            if(f)this.config.local.ignored=f;
        }catch(e){
            console.warn("No ignore file");
        }
    }

    /**
     * Verify config directory exists
     * @returns {Promise<undefined>} Void promise
     */
    verifyDirs(){
        return fs.ensureDir(path.resolve(this.base));
    }

    /**
     * Authentication scopes needed by JDrive
     * @readonly
     * @returns {string[]} Authentication scopes
     */
    get scopes(){
        return ['https://www.googleapis.com/auth/drive'];
    }
}

module.exports=Config;
