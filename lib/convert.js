const regexEscape=require("escape-string-regexp");
const pathModule=require("path");

/**
 * Conversion module between Drive and the Local Filesystem
 * @module lib/convert
 */

/**
 * @param {Object} f - Drive file object
 * @param {string} f.mimeType - File mimeType
 * @returns {boolean} True if it is a directory, false elsewhere
 */
function isDirectory(f){
    return f.mimeType=="application/vnd.google-apps.folder";
}

/**
 * Drive file resource
 * @typedef {Object} DriveFile
 * @property {string} path - Path to local counterpart
 */

/**
 * Drive change for a determinate resource
 * @typedef {Object} DriveChange
 */

class Hierarchy{

    /**
     * @param {Object} root - Reference to root object on Drive
     * @param {string} root.rootId - Root element ID
     */
    constructor(root){
        this.root={};
        this.rootId=root.id;
        this.rootEl=root;
    }

    /**
     * Populates this.eqivalent with modified Drive file objects
     * @param {Array} all - Array containing all of the elements in Drive
     * @returns {undefined} Void
     */
    create(all){
        this.all=all;
        let eq=[];
        for(let i in all){
            if(!all.hasOwnProperty(i))continue;
            let obj=all[i];
            obj.path=this.genChain(all[i],true,"/");
            if(obj.path===false)continue;
            eq.push(obj);
        }
        if(this.equivalent)console.warn("Conversion tree forced to update")
        else console.log("Conversion tree generated");
        this.equivalent=eq;
        Reflect.deleteProperty(this,'all'); //It's not needed now
    }

    /**
     * Generates path for a given file
     * @param {DriveFile} element - The drive file element
     * @param {boolean} alt - Use names instead of IDs on the path
     * @param {string} separator - Use this character as separator in the path
     * @returns {string} File path
     */
    genChain(element,alt,separator="/"){
        var actualElement=element;
        var chain='';
        while(true){
            if(actualElement.id==this.rootId){
                if(!alt)chain=`${actualElement.id}${chain!=''?separator+chain:chain}`;
                break;
            }else{
                chain=`/${separator}${alt?actualElement.name:actualElement.id}${chain!=''?separator:''}${chain}`;
                //if(actualElement.parents.length>1)debugger; //FIXME it's a rare case, so test is needed
                if(!actualElement.parents&&actualElement.isSharedWithMe){
                    actualElement=this.rootEl;
                    continue;
                }
                if(!actualElement.parents)return false; //Return false when it has no parents at all
                let parent=this.get(actualElement.parents[0]);
                if(!parent)return false; //Orphan element
                actualElement=parent;
            }
        }
        return pathModule.resolve(chain);
    }

    /**
     * Get a file from the local conversion tree
     * @param {any} value - The value of a giver proverty on the file
     * @param {string} field - The field we are looking for
     * @returns {DriveFile} A drive file object
     */
    get(value,field="id"){
        if(field=="id"&&value==this.rootId)return this.rootEl;
        if(field=="name"&&value=="root")return this.rootEl;
        if(field=="path"&&value=="/")return this.rootEl;
        let n=this.equivalent?"equivalent":"all";
        let rt=this[n].filter(f=>f[field]==value)[0];
        return rt||false;
    }

    /**
     * Check if a file parents have been created
     * @param {string} path - File path
     * @returns {boolean} True if parent exists, false otherwise
     */
    parentCreated(path){
        if(!path)return true; //What are you trying to upload?
        let layers=path.split("/").filter(p=>p!="");
        if(layers.length<2)return true; //It is on root directory
        for(let layerN=0,ln=layers.length-1; layerN<ln; layerN++){
            if(!layers.hasOwnProperty(layerN))continue;
            let actual=`/${layers.slice(0,layerN+1).join("/")}`;
            let exists=this.get(actual,"path");
            if(exists===false)return false;
        }
        return true;
    }

    /**
     * Add a file to the conversion tree
     * @param {DriveChange} change - Drive change event object
     * @returns {undefined} Nothing is returned
     */
    addFile(change){
        if(this.get(change.file.id))return this[isDirectory(change.file)?'updateDir':'updateFile'](change);
        let path=this.genChain(change.file,true,"/");
        if(path===false)throw new Error("Couldn't add "+change.file.name+" to conversion tree"); //Orphan element
        let rt=change.file;
        rt.path=path;
        this.equivalent.push(rt);
    }

    /**
     * Remove a file from the conversion tree
     * @param {DriveChange} change - Drive change event object
     * @returns {boolean} True or False depending on the result of property deletion
     */
    deleteFile(change){
        let {file}=change;
        let td=this.get(file.id);
        let index=this.equivalent.indexOf(td);
        if(index===-1)return; //It doesn't exist, so it's kind of "deleted"
        return Reflect.deleteProperty(this.equivalent,index);
    }

    /**
     * Update a file in the conversion tree
     * @param {DriveChange} change - Drive change event object
     * @returns {boolean} True or False depending on the result of the update
     */
    updateFile(change){
        let {file}=change;
        let index=this.equivalent.indexOf(this.get(file.id));
        file.path=this.genChain(file,true,"/");
        if(file.path===false)throw new Error("Orphan element "+change.file.name+", so there was an error adding it to the conversion tree");
        if(index!==-1){
            this.equivalent[index]=file;
            return true;
        }
        return false;
    }

    /**
     * Update a directory (and all of the dependents) in the conversion tree
     * @param {DriveChange} change - Drive change event object
     * @returns {boolean} True or False depending on the result of the update
     */
    updateDir(change){
        let {file}=change;
        let old=this.get(file.id);
        let children=this.equivalent.filter(f=>f.path!==old.path&&f.path.match(regexEscape(old.path))).sort((a,b)=>a.path.length-b.path.length);
        this.updateFile({file});
        for(let i in children){
            if(!children.hasOwnProperty(i))continue;
            let child=children[i];
            if(isDirectory(child)){
                this.updateDir({file:child});
                continue;
            }
            this.updateFile({file:child});
        }
        return true;
    }

    /**
     * Conversion tree event receiver
     * @param {string} type - Type of event from Drive
     * @param {DriveChange} change - Drive change event object
     * @returns {any} Depends on the executed function
     */
    change(type,change){
        if(change.file&&change.file.name&&global.isDev)console.log(type,change.file.name);
        try{
            switch(type){
                case "unlinkDir":
                case "unlink":
                    return this.deleteFile(change);
                case "add":
                case "addDir":
                    return this.addFile(change);
                case "change":
                case "move":
                    return this.updateFile(change);
                case "moveDir":
                    return this.updateDir(change);
                default:
                    return console.error(new Error(`Unknown event ${type} from remote at tree conversion`),change);
            }
        }catch(e){
            console.error(e); //Don't stop
        }
    }
}

module.exports={
    Hierarchy
};
