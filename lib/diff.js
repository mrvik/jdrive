const _=require("lodash");
const anymatch=require("anymatch");

/**
 * Diff module
 * @module lib/diff
 */

function isDirectory(f){
    return f.mimeType=="application/vnd.google-apps.folder";
}

/**Does a diff of two file trees*/
class Diff{

    /**
     * @param {Object} files - Files object
     * @param {Array} files.a - File tree a
     * @param {Array} files.b - File tree b
     * @param {Array} files.events - Events emitted from tree a
     * @param {Array} ignore - Ignore list when doing diff
     */
    constructor(files,ignore){
        let matchers=[];
        if(typeof ignore=="object"){
            for(let i in ignore){
                if(!ignore.hasOwnProperty(i))continue;
                matchers.push(new RegExp(ignore[i]));
            }
        }else if(typeof ignore!="undefined"){
            matchers.push(new RegExp(ignore));
        }
        this.matcher=anymatch(matchers);
        let {a,b,events}=files;
        this.a=a.map(this.extract).filter(f=>!this.matcher(f.path));
        this.b=b.map(this.extract).filter(f=>!this.matcher(f.path));
        this.events=events.sort((ea,eb)=>Number(new Date(eb.time))-Number(new Date(ea.time)));
        this.init();
    }

    /**
     * Does the diff of the two hierarchies and merges them. Then stores it into this.diff
     * @returns {undefined} Void
     */
    init(){
        let LrmEvents=this.applyEvents(this.b).map(this.toEvent);
        let Rdiff=_.differenceWith(this.a,this.b,this.compare);
        let Ldiff=_.differenceWith(this.b,this.a,this.compare);
        this.REvents=Rdiff
            .map(this.toEvent)
            .concat(LrmEvents)
            .sort(this.sortEvents);
        this.LEvents=Ldiff
            .map(this.toEvent)
            .sort(this.sortEvents);
    }

    /**
     * Prepares files to the diff function
     * @param {Object} r - Original file
     * @param {string} r.path - File path
     * @param {Object} r.stats - File stat
     * @param {string} r.modifiedTime - Modification date as ISO string
     * @returns {Object} An object with path and modification time (as number) properties
     */
    extract(r){
        let {stats,modifiedTime,id}=r;
        let time=typeof stats=="undefined"?new Date(modifiedTime):stats.mtimeMs;
        time=Number(time);
        let _isDirectory=r.isDirectory;
        if(id)_isDirectory=isDirectory(r);
        return {time,isDirectory:_isDirectory,...r};
    }

    /**
     * Sort events by path length (ascending)
     * @param {Object} evA - First event
     * @param {Object} evB - Second event
     * @returns {number} evA comes first than evB
     */
    sortEvents(evA,evB){
        if(evA[1].file&&evA[1].file.path){
            if(evB[1].file&&evB[1].file.path){
                return evA[1].file.path.length-evB[1].file.path.length;
            }
            return -1;
        }
        return 1;
    }

    /**
     * Apply unlink events to the parameter
     * @param {Array} to - Array of changed files
     * @returns {Array} Remove events
     */
    applyEvents(to){
        let rt=[];
        this.events.forEach(ev=>{
            let [type,change]=ev;
            change.time=Number(new Date(change.time));
            if(!change.file)return;
            let item=to.filter(t=>t.path==change.file.path)[0];
            if(!item)return;
            let index=to.indexOf(item);
            switch(type){
                case "unlink":
                case "unlinkDir": {
                    if(item.time>change.time)break;
                    let path=item.path;
                    to.splice(index,1);
                    rt.push({path,time:change.time,id:change.fileId,type});
                    break;
                }
                default:
                    //What the fsck is this?
            }
        });
        return rt;
    }

    /**
     * Convert a change to a cross-handler event
     * @param {Object} change - The diff change object
     * @see Events.js~onRemote
     * @see Events.js~onLocal
     * @returns {Object} Cross handler event (Remote and Local)
     */
    toEvent(change){
        let type=change.type||"change"; //Add or change are processed the same way
        if(change.isDirectory)type="addDir"; //There's no 'changeDir' event
        let {path,time}=change;
        let name=path.split("/")
            .filter(p=>p!="")
            .splice(-1,1)[0];
        let file={name,...change};
        let old=file;
        return [type,{file,old,time}];
    }

    /**
     * Compares two files
     * @see extract
     * @param {Object} a - File a (modified with extract)
     * @param {Object} b - File b (modified with extract)
     * @returns {boolean} True when those are equal or false when different
     */
    compare(a,b){
        //True ignores the property and false adds it to the diff
        if(a.path==b.path){
            let ta=Math.trunc(Number(a.time)); //Sometimes, the local files are floats
            let tb=Math.trunc(Number(b.time));
            if(ta<=tb)return true; //a is older or equal than b
        }
        return false;
    }
}

module.exports=Diff;
