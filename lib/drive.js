/* eslint max-lines: ["off"] */
const {google}=require("googleapis");
const {Hierarchy}=require("./convert");
const Queue=require("./queue");

/**
 * Drive module
 * @module lib/drive
 */

/**
 * Check if a DriveFile is a directory
 * @param {Object} f - Drive File to check
 * @returns {boolean} True or false depending on f is directory
 */
function isDirectory(f){
    return f.mimeType=="application/vnd.google-apps.folder";
}

/**
 * Check wether a DriveFile is a Google Apps file
 * @param {Object} file - Drive file to check
 * @returns {boolean} True if the file is a GoogleDoc, false otherwise
 */
function isGoogleApp(file){
    let reg=/(application\/vnd\.google-apps\.)([a-z-]*)/;
    return file.mimeType.match(reg)!==null;
}

/**
 * OAuth2 object generated on auth.js
 * @typedef {Object} OAuth2
 */

/**
 * The class itself
 * @typedef {Object} this
 * @see Drive
 */

/**
 * Function to be called from event dispatcher
 * @typedef {Function} EventCallback
 */

/**
 * FSDriver class
 * @typedef {Object} FSDriver
 * @see localfs.js
 */

/**Does all the work related to Google Drive API*/
class Drive{

    /**
     * @param {OAuth2} a - Oauth2 client
     * @param {Object} options - Options to configure the class
     */
    constructor(a,options){
        this.config=options.config||{};
        this.hooks={};
        this.fields=["id","name","mimeType","trashed","permissions","parents","webContentLink","viewedByMeTime","createdTime","modifiedTime","ownedByMe","size"];
        this.lock=this.init(a);
        this.queue=new Queue(this.config.workers);
    }

    /**
     * Make the class start working
     * @async
     * @param {OAuth2} auth - Oauth2 client
     * @returns {Promise<undefined>} Fullfills when the first (blocking) work is done
     */
    async init(auth){
        await global.lockfile.lock;
        if(!global.lockfile.contents.drive){
            global.lockfile.contents.drive={};
        }
        this.lockfile=global.lockfile.contents.drive;
        global.lockfile.updateLock();
        this._drive=google.drive({version: 'v3',auth});
        this.root=await this.stat("root");
        this.permissionId=this.root.permissions.filter(p=>p.role=="owner")[0].id;
        this.tree=await this.ls();
        this.hierarchy=new Hierarchy(this.root);
        this.hierarchy.create(this.tree);
        this.on("all", (...args)=>this.hierarchy.change(...args));
    }

    /**
     * Start watching Google Drive events
     * @async
     * @returns {boolean} Always true when the watching is interrupted
     */
    async startWatching(){
        if(!this.startPageToken)this.startPageToken=await this.getStartPageToken();
        this.watcher=this.watch();
        return true;
    }

    /**
     * Get a list of Google Drive files
     * @async
     * @returns {Promise<Array>} - Filtered array of files
     */
    async ls(){
        let files=[];
        let nextPageToken;
        do{
            let p=await this._drive.files.list({
                fields: `nextPageToken,files(${this.fields.join(",")})`,
                pageSize: 100,
                spaces: "drive",
                q: "trashed!=true",
                pageToken:nextPageToken
            });
            let {data}=p;
            files=files.concat(data.files);
            nextPageToken=data.nextPageToken;
        }while(nextPageToken);
        console.log(`Remote tree map has ${files.length} files`);
        let filtered=files.filter(f=>this.filterFile(f));
        return filtered;
    }

    /**
     * Filter the files that jdrive can handle. Also modifies some file's metadata
     * @param {Object} file - File to filter
     * @param {string} [origin='list'] - Where is the function called from
     * @returns {boolean} True if file can be included in the tree. False elsewhere
     */
    filterFile(file,origin='list'){
        if(origin!=="event"){ //If the file is from a event, it can be deleted
            if(file.trashed)return false;
        }
        if(isGoogleApp(file)&&!isDirectory(file)){
            if(!this.config.allowDriveFiles)return false;
        }
        if(file.ownedByMe)return true; //If the user is the owner no further check is needed
        if(this.config.allowSharedFiles){ //If this config is enabled we should check if this file is owned by the user
            file.isSharedWithMe=true;
            if(!file.permissions)return false;
            let perm=file.permissions.filter(p=>{
                if(!p.deleted&&p.id==this.permissionId&&(p.role=="owner"||p.role=="writer"))return true;
                return false;
            })[0];
            if(perm){
                //Shared files have no parents in the Drive filesystem, so we add them to the root folder.
                if(!file.parents)file.parents=[this.root.id];
                return true;
            }
        }
        return false;
    }

    /**
     * Get a file's metadata (by ID)
     * @async
     * @param {string} f - FileID
     * @throws Error when the file is not found
     * @returns {Promise<Object>} FileObject
     */
    async stat(f){
        let p=await new Promise((resolve,reject)=>{
            this._drive.files.get({
                fields: this.fields.join(","),
                fileId: f
            },(err,res)=>{
                if(err)return reject(err);
                return resolve(res);
            });
        });
        return p.data;
    }

    /**
     * Watch drive for changes. Stops with 'this.stop==true'
     * @async
     * @returns {Promise<undefined>} Fullfills when it stops watching
     */
    async watch(){
        while(true){
            if(this.stop)break;
            await this.fetchChanges();
        }
    }

    /**
     * Add a event listener to Drive
     * @see dispatch
     * @param {(string|string[])} event - Event(s) to listen for
     * @param {EventCallback} callback - Callback for event
     * @returns {this} The class itself (allows chaining)
     */
    on(event,callback){
        if(typeof event=="object")return event.forEach(evt=>this.on(evt, callback));
        if(this.hooks[event]){
            this.hooks[event].push(callback);
        }else{
            this.hooks[event]=[callback];
        }
        return this;
    }

    /**
     * Dispatch a event from Drive
     * @async
     * @see on
     * @param {(string|string[])} event - Event(s) to be dispatched
     * @param {Object} args - Drive change object
     * @returns {Promise<(number|undefined)>} The number of hooks executed. Undefined if there are no hooks
     */
    async dispatch(event,args){
        let hooks=[];
        if(typeof event == "object")return event.forEach(ev=>this.dispatch(ev,args));
        if(this.hooks.all){
            this.hooks.all.forEach(ev=>hooks.push(ev(event,args))); //Dispatch the event to the catchall callbacks
        }
        if(event!="all"&&this.hooks[event]){
            this.hooks[event].forEach(ev=>hooks.push(ev(args)));
        }
        await Promise.all(hooks);
        return hooks.length;
    }

    /**
     * Create a event from a Drive change
     * @param {Object} change - Drive change event
     * @returns {(string|string[])} Name(s) of the event
     */
    createEvent(change){
        //Please, note that to avoid confusion, the events should be the same as 'localfs'
        const {file}=change;
        //Some elements are removed directly (not trashed)
        if(change.removed){
            let cached=this.hierarchy.get(change.fileId);
            if(cached){
                change.old=cached;
                change.file=cached;
                if(!isDirectory(cached))return "unlink";
                return "unlinkDir"; //unlinkDir as default, it will trigger a rimraf and a tree lookup, that's better than forget files in the tree
            }
            return false; //If it's removed but not cached, maybe it's not allowed to fire an event
        }
        if(file){
            if(!this.filterFile(file,'event'))return false; //This file is not allowed to fire an event
            let cachedFile=this.hierarchy.get(file.id);
            if(file.trashed||!file.parents){ //Remove file/directory. If parents array is empty, it should be deleted
                if(cachedFile){
                    change.old=cachedFile;
                }else{
                    let path=this.hierarchy.genChain(file,true);
                    change.old={path};
                    change.file.path=path;
                }
                return isDirectory(file)?"unlinkDir":"unlink";
            }
            if(cachedFile!=false){
                let oldDate=cachedFile.modifiedTime;
                let newDate=file.modifiedTime;
                if(cachedFile.name!=file.name||cachedFile.parents[0]!=file.parents[0]){ // Catch file moves by looking for their ID.
                    change.old=cachedFile;
                    return isDirectory(file)?"moveDir":"move";
                }
                if(oldDate==newDate)return false; // The file didn't change
                change.path=cachedFile.path;
                //Goes to 'change' event below
            }else{
                if(isDirectory(file)){
                    return "addDir";
                }
                return "add";
            }
            return "change"; //The change event as a fallback
        }
        return "all"; //What the fsck? IDK what happened, but a event should be fired anyway...
    }

    /**
     * Fetch changes from Drive with the pageToken
     * @async
     * @returns {Promise<undefined>}  If there's no changes, waits 2 additional seconds to fullfill
     */
    async fetchChanges(){
        let pageToken=this.startPageToken;
        let data=await this.changesFrom(pageToken);
        if(data.newStartPageToken)this.startPageToken=data.newStartPageToken;
        let {changes}=data;
        if(changes.length){
            let dpers=[];
            changes.forEach(change=>{
                let event=this.createEvent(change);
                if(event===false)return;
                dpers.push(this.dispatch(event,change)); //This function is async but we won't wait for it to be completed.
            });
            Promise.all(dpers).then(()=>{
                if(global.lockfile.contents.drive.pageToken!=data.newStartPageToken){
                    global.lockfile.contents.drive.pageToken=data.newStartPageToken;
                    return global.lockfile.updateLock();
                }
            });
        }else{
            await new Promise(r=>setTimeout(r,2000));
        }
    }

    /**
     * Get lost events when off
     * @async
     * @returns {Promise<Array>} Events array
     */
    async getLostEvents(){
        let pt=global.lockfile.contents.drive.pageToken;
        if(!pt)return []; //No page token not events are lost
        return await this.changesFrom(pt).then(evs=>{
            this.startPageToken=evs.newStartPageToken;
            return evs.changes.map(ev=>{
                let type=this.createEvent(ev);
                if(type)return [type, ev];
                return false;
            }).filter(r=>r!==false);
        });
    }

    /**
     * @async
     * @param {string} pageToken - Page token to query about
     * @returns {Promise<Object>} Object with changes and next page token
     */
    async changesFrom(pageToken){
        let changes=[];
        let newStartPageToken,nextPageToken;
        if(!pageToken)throw new Error("No page token");
        do{
            let {data}=await this._drive.changes.list({
                pageToken:nextPageToken||pageToken,
                fields: '*'
            });
            nextPageToken=data.nextPageToken;
            newStartPageToken=data.newStartPageToken;
            if(data.changes)changes=changes.concat(data.changes);
        }while(nextPageToken);
        return {changes,newStartPageToken};
    }

    /**
     * Get start page token to start watching drive
     * @async
     * @returns {Promise<string>} Start page token to drive.changes
     */
    async getStartPageToken(){
        let {data}=await this._drive.changes.getStartPageToken({});
        global.lockfile.contents.drive.pageToken=data.startPageToken;
        global.lockfile.updateLock();
        return data.startPageToken;
    }

    /**
     * Register FS Driver into Drive
     * @see module:localfs
     * @param {FSDriver} driver - Localfs instance
     * @returns {undefined} Void
     */
    registerFS(driver){
        this.fsDriver=driver;
    }

    /**
     * Sync everything in the list
     * @async
     * @see downloadList
     * @returns {Promise<boolean>} True if the sync is OK
     */
    async syncEverything(){
        if(this.fsDriver.lock)await this.fsDriver.lock;
        let files=this.getFileList;
        let dirs=this.getDirList;
        return await this.downloadList({files,dirs});
    }

    /**
     * Sync a diff from the first parameter
     * @async
     * @param {Array} diff - The diff tree array
     * @returns {Promise<boolean[]>} Result of the download and upload operations on this order.
     * @see downloadList
     * @see uploadList
     */
    async syncDiff(diff){
        let all=diff.filter(f=>f.location=='remote').map(f=>{
            return this.hierarchy.get(f.path,"path");
        });
        let local=diff.filter(f=>f.location=='local').map(f=>{
            let stat=this.fsDriver.stat(f.path);
            f.atime=stat.atimeMs;
            f.mtime=stat.mtimeMs;
            f.ctime=stat.ctimeMs;
            f.isDirectory=stat.isDirectory();
            return f;
        }); //Local dirs and files
        let files=all.filter(f=>!isDirectory(f)); //Remote dirs and files
        let dirs=all.filter(isDirectory);
        let ldirs=local.filter(f=>f.isDirectory);
        let lfiles=local.filter(f=>!f.isDirectory);
        var dl=await this.downloadList({dirs,files});
        if(this.stop)return false;
        var ul=await this.uploadList({
            dirs:ldirs,
            files:lfiles
        });
        if(ul===false)return false;
        return [dl,ul];
    }

    /**
     * Upload a list of files provided by parameter
     * @async
     * @param {Array} list - The list of files to upload
     * @param {boolean} skipBodies - Skip file contents upload
     * @see createDir
     * @see uploadFile
     * @returns {Promise<boolean>} Evaluated as true when the sync is OK
     * @throws Errors from Drive
     */
    async uploadList(list,skipBodies){
        let {dirs,files}=list;
        let queuedFiles=[];
        if(dirs&&dirs.length>0){
            for(let i in dirs){
                if(!dirs.hasOwnProperty(i))continue;
                if(this.stop)return false; //Inmediate exit
                let d=dirs[i];
                console.log("Creating "+d.path);
                await this.createDir(d)
                    .then(f=>{ //This also contains the atime and mtime
                        if(f!==false)console.log("Create OK ",d.path);
                        return f;
                    })
                    .catch(console.error);
            }
        }
        if(files&&files.length>0){
            let fn=this.uploadFile.bind(this);
            for(let i in files){
                if(!files.hasOwnProperty(i))continue;
                if(this.stop)return false; //Inmediate exit
                let f=files[i];
                if(!skipBodies)console.log("Uploading "+f.path);
                else console.log("Updating metadata for "+f.path);
                let progress=new Promise((callback,callbackError)=>{
                    let item={fn,args: [f,skipBodies],callback,callbackError};
                    this.queue.push(item);
                })
                    .then(r=>{
                        //eslint-disable-next-line no-nested-ternary
                        if(r)console.log("Upload OK",r.path?r.path:r.name?r.name:r); //Log path, name or return value on this order
                        return r;
                    })
                    .catch(console.error);
                queuedFiles.push(progress);
            }
        }
        await Promise.race([Promise.all(queuedFiles),Promise.all(this.queue.workers)]);
        return !this.stop;
    }

    /**
     * Creates a drive resource from the fie parameter
     * @param {Object} f - The file properties (including path and stat)
     * @returns {Object} Drive resource
     */
    createResource(f){
        let spl=f.path.split("/");
        let parent=spl.slice(0,-1).join("/");
        let name=spl[spl.length-1];
        let resource={
            name,
            viewedByMeTime: new Date(f.atime).toISOString(),
            createdTime: f.fileId?undefined:new Date(f.ctime).toISOString(), //eslint-disable-line no-undefined
            modifiedTime: new Date(f.mtime).toISOString()
        };
        let args=[f.path,"path"];
        if(f.fileId)args=[f.fileId,"id"];
        let old=this.hierarchy.get(...args);
        if(parent!="")resource.parents=[this.hierarchy.get(parent,"path").id];
        if(f.mimeType)resource.mimeType=f.mimeType;
        if(f.fileId){
            if(old&&old.parents&&resource.parents&&resource.parents[0]!=old.parents[0]){ //Parents exists and differs
                resource.addParents=resource.parents;
                resource.removeParents=old.parents;
            }
            resource.fileId=f.fileId;
            if(resource.parents)Reflect.deleteProperty(resource,'parents');
        }
        return resource;
    }

    /**
     * Create a directory on Drive
     * @param {Object} dir - Drive resource of the new directory
     * @see createFile
     * @returns {Promise<Object>} - Complete file resource from Drive
     */
    createDir(dir){
        let cached=this.hierarchy.get(dir.path,"path");
        dir.mimeType='application/vnd.google-apps.folder';
        if(cached!==false){ //Ensure it doesn't exist on Drive to avoid problems
            dir.fileId=cached.id;
        }
        let resource=this.createResource(dir);
        let args={resource};
        return this.createFile(args); //A directory is a file with a special mimeType ;)
    }

    /**
     * Prepare a resource for upload
     * @async
     * @param {Object} file - Drive resource object
     * @param {boolean} skipBody - Skip file read. Update metadata only
     * @see createFile
     * @returns {Promise<Object>} - Complete file resource form Drive
     */
    async uploadFile(file,skipBody){
        var {fileId}=file;
        if(!fileId){
            //We get here the fileId of a possible file because the file may not be created on the trigger phase and have no fileId, but now it should
            //So here we get the fileId just before creating the Drive resource to ensure we have the right fileId
            let cached=this.hierarchy.get(file.path,"path");
            if(cached){
                fileId=cached.id;
                file.fileId=fileId;
            }
        }
        let resource=this.createResource(file);
        let media;
        if(!skipBody){
            let e=await this.fsDriver.exists(file.path);
            if(e===false){
                let err=new Error(`File ${file.path} doesn't exist`);
                err.code="ENOENT";
                throw err;
            }
            media={
                body: this.fsDriver.createReadStream(file.path,file.fd)
            }
        }
        return await this.createFile({resource,media,fileId});
    }

    /**
     * Create a file on Drive
     * @async
     * @param {Object} f - The Drive resource to create file
     * @throws Drive errors
     * @returns {Promise<Object>} - Complete file resource form Drive
     */
    async createFile(f){ //Create or update file
        let {resource,media,fileId}=f;
        if(typeof resource.trashed=="undefined"&&fileId)resource.trashed=false; //When updating a file, should not be trashed (as it's been updated) except if trashed is explicitly set
        let {addParents,removeParents}=resource;
        if(media)media.body.on('error',e=>{
            return Promise.reject(e);
        });
        return await this._drive.files[fileId?'update':'create']({ //If the fileId is provided, the file will be treated in a different way
            fileId,
            resource,
            addParents,
            removeParents,
            media,
            fields: this.fields.join(",")
        })
            .then(fi=>{
                let {data}=fi;
                this.filterFile(data);
                try{
                    let option="addFile";
                    if(fileId)option=isDirectory(f.resource)?"updateDir":"updateFile";
                    if(data.trashed)option="deleteFile";
                    this.hierarchy[option]({file:data});
                }catch(e){
                    this.updateTree(); //A stale tree can cause some errors
                }
                return data;
            });
    }

    /**
     * Download a list of files from Drive
     * @async
     * @param {Object} list - Resources to download
     * @param {Array} list.dirs - Directories to create
     * @param {Array} list.files - Files to download
     * @returns {boolean} True when everything is downloaded ok
     */
    async downloadList(list){
        let {dirs,files}=list;
        let queuedFiles=[];
        for(let i in dirs){ //Dirs doesn't need a queue
            if(!dirs.hasOwnProperty(i))continue;
            if(this.stop)break; //Inmediate exit
            await this.fsDriver.ensure(dirs[i].path);
            await this.fsDriver.attr(dirs[i]);
        }
        for(let i in files){
            if(!files.hasOwnProperty(i))continue;
            if(this.stop)console.warn("Stopping processsss");
            if(this.stop)break; //Interrupt process
            let progress=new Promise((callback,callbackError)=>{
                let item={
                    fn: this.downloadFile.bind(this),
                    args:[files[i]],
                    callback,
                    callbackError
                };
                this.queue.push(item);
            })
                .then(f=>{
                    if(f)return console.log("Sync OK", f)
                });
            queuedFiles.push(progress);
        }
        await Promise.race([Promise.all(queuedFiles),Promise.all(this.queue.workers)]);
        return !this.stop;
    }

    /**
     * Download a file from Drive. Pipes download to file.path and sets atime and mtime
     * @async
     * @param {Object} file - Drive file resource
     * @throws Drive errors
     * @returns {Promise<boolean>} Fulfills when the file has been downloaded and attributes set
     */
    async downloadFile(file){ //eslint-disable-line max-lines-per-function
        console.log("Syncing ",file.path);
        if(file.webContentLink){
            let rt=await this._drive.files.get(
                {fileId:file.id,alt:'media'},
                {responseType:'stream'}
            ).catch(e=>{
                if(!e.code)throw e;
                if(e.code>=400){
                    console.error(`Couldn't download ${file.path} due to status ${e.code}: ${e.response?e.response.statusText:''}`);
                    return "error";
                }
                throw e;
            });
            if(rt=="error")return false;//There has been an error
            let p=file.path.split("/");
            p.splice(-1,1);
            await this.fsDriver.ensure(p.join("/"));
            var pipe=this.fsDriver.createStream(file.path);
            console.log("Writing ",file.path);
            return await new Promise((r,j)=>{
                rt.data
                    .on('error',j) //The error can be thrown by the write function
                    .pipe(pipe);
                pipe.on('finish',r); //Pipe finish event is fired from the pipe when data is flushed to storage
            })
                .then(()=>{
                    console.log("Apply attributes for ",file.path);
                    return this.fsDriver.attr(file);
                })
                .then(()=>{
                    return file.path;
                })
                .catch(e=>{
                    console.error("Failed to download "+file.path,e);
                    return this.fsDriver.exists(file.path).then(res=>{
                        if(!res)return;
                        return this.fsDriver.remove(file.path);
                    });
                });
        }
        return true; //Forget Google Docs, those can be accessed from the web
    }

    /**
     * Remove a file from Google Drive
     * @async
     * @param {Object} file - Drive file resource
     * @throws Drive error
     * @returns {Promise<Object>} Drive resource for the deleted file
     */
    async deleteFile(file){
        let res=this.hierarchy.get(file.path,"path");
        if(res===false)return res;
        let fileId=res.id;
        if(this.config.deleteForever){
            return await this._drive.files.delete({
                fileId
            }).then(()=>{
                return this.hierarchy.deleteFile({file:res});
            })
        }
        let resource={trashed:true};
        return await this.createFile({fileId,resource});
    }

    /**
     * Get directory list
     * @returns {Array} Filtered directory list
     */
    get getDirList(){
        return this.hierarchy.equivalent.filter(isDirectory);
    }

    /**
     * Get file list
     * @returns {Array} Filtered file list (no directories)
     */
    get getFileList(){
        return this.hierarchy.equivalent.filter(f=>!isDirectory(f));
    }

    /**
     * Update the Drive tree and the conversion tree
     * @returns {undefined} void
     */
    async updateTree(){
        let now=Number(Date.now());
        let lastUpdate=this.latestTreeUpdate||0;
        if(now-lastUpdate<4000)await new Promise(r=>setTimeout(r,now-lastUpdate));
        this.tree=await this.ls();
        this.hierarchy.create(this.tree);
        this.latestTreeUpdate=Number(Date.now());
    }

    set stop(value){
        this._stop=value;
        this.queue.stop=value;
    }

    get stop(){
        return this._stop;
    }
}

module.exports={
    Drive,
    isDirectory
}
