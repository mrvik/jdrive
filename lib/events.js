/* eslint max-lines: ["error", 600] max-lines-per-function: ["error",{max:60,skipBlankLines:true,skipComments:true}] */
const Queue=require("./queue");
const Socket=require("./socket");

/**
 * Events module. Handles local and remote events
 * @module lib/events
 */

var {isDirectory}=require("./drive");

/**Events handler class*/
class Events{

    /**
     * @param {Object} a - Configuration objects
     * @param {Object} a.remote - Remote class object
     * @param {Object} a.local - Local class object
     * @param {Object} a.config - Configuration for events class
     * @see module:lib/localfs
     * @see module:lib/drive
     */
    constructor(a){
        let {remote,local,config}=a;
        this.remote=remote;
        this.local=local;
        this.config=config||{};
        this.blacklist=[];
        this.remoteBlacklist=[];
        this.moving=[];
        this.localFiles={};
        this.queue=new Queue(this.config.workers);
        this.dependentTypes=["add","addDir","change","move"];
        //move event is needed here as there may be a 'move' event followed by a 'change' one but the first one checks if uploaded file is OK
    }

    /**
     * Catch remote events and add to queue
     * @param {...Object} args - Arguments to the function
     * @see _onRemote for parameters and return value
     * @returns {Promise<any>} - Return value of _onRemote
     */
    onRemote(...args){
        let fn=this._onRemote.bind(this);
        return new Promise((r,j)=>{
            let item={
                fn,
                args,
                mode: "download",
                type: args[0],
                file: args[1].file.name,
                callback:r,
                callbackError:j
            }
            this.queue.push(item);
        }).catch(e=>{
            if(e.code==="ENOENT")return; //Avoid throwing ENOENT errors
            throw e;
        })
    }

    /**
     * Handle remote events (dont' call directly)
     * @async
     * @param {string} type - Event type
     * @param {object} event - Drive file change
     * @see module:lib/drive.createEvent
     * @returns {Promise<any>} void
     */
    //Function allowed to be complex
    //eslint-disable-next-line complexity
    async _onRemote(type,event){ //eslint-disable-line max-lines-per-function
        if(event.file&&event.file.name)console.log(`Events catcher, ${type}`,event.file.name);
        let evPath=(event.old&&event.old.path?event.old.path:event.path)||this.remote.hierarchy.get(event.file.id).path;
        //If the event path is not defined, update the conversion tree and look again.
        //If file is not present in remote tree, return and throw a warning
        if(!evPath)if(!evPath)return console.info("Couldn't find target "+event.file.name+" in remote tree");
        if(this.isRemoteBlacklisted(evPath)){
            //console.log("Blacklisted remote event "+type, event.file.name);
            return;
        }
        switch(type){
            case "unlink":
            case "unlinkDir": {
                let localStat={};
                let {old}=event;
                if(!old){ //Maybe it's been deleted
                    return true;
                }
                if(!old.path)throw new Error(`Failed to determine path to ${event.file.name} ID:${event.file.id}`);
                try{
                    localStat=this.local.stat(old.path);
                }catch(e){
                    if(e.code=="ENOENT")return true; //File doesn't exist
                }
                if(localStat.mtime){
                    let thisTime=Number(localStat.mtime);
                    let upstreamDate=Number(new Date(event.time));
                    if(thisTime>upstreamDate)return true;
                }
                this.blacklistFile(old.path);
                return await this.local.remove(old.path)
                    .catch(e=>this.catcher("remote",type,old.path,e))
                    .finally(async ()=>{
                        await new Promise(r=>setTimeout(r,500)); //Localfs watcher takes some time to detect some events
                        return this.blacklistFile(old.path,true);
                    });
            }
            case "add":
            case "addDir":
            case "change": {
                let rq={};
                let {path}=event.file;
                if(!path)path=evPath; //It's a rare but possible case
                if(this.isBlacklisted(path))return; //Don't do anything if the file is blacklisted
                this.blacklistFile(path);
                event.file.path=path;
                if(isDirectory(event.file)){
                    rq.dirs=[event.file]
                }else{
                    rq.files=[event.file]
                }
                return await this.remote.downloadList(rq)
                    .catch(e=>this.catcher("remote",type,path,e))
                    .finally(async ()=>{
                        await new Promise(r=>setTimeout(r,500)); //Same reason as above
                        return this.blacklistFile(path,true);
                    });
            }
            case "move":
            case "moveDir": {
                let {old}=event;
                let file=this.remote.hierarchy.get(event.file.id);
                this.blacklistFile(old.path);
                this.blacklistFile(file.path);
                return await this.local.move({old,file}).finally(async ()=>{
                    await new Promise(r=>setTimeout(r,500)); //The same
                    this.blacklistFile(old.path,true);
                    this.blacklistFile(file.path,true);
                });
            }
            case "all":
                console.warn("'all' event fired, IDK why, but thats it: ",event);
                break;
            default:
                throw new Error(`Unrecognized event ${type}`,event);
        }
    }

    /**
     * Manage a given local event. See triggerLocal and _onLocal for parameters
     * @param {string} type - Event type
     * @param {string} file - File path
     * @param {Object} stat - File stat
     * @see _onLocal
     * @see triggerLocal
     * @returns {Promise<any>} - A Promise chain (use with await!)
     */
    onLocal(type,file,stat){ //This is a sync function by design to prevent race conditions
        if(this.isBlacklisted(file))return;
        if(this.localFiles[file]){
            var f=this.localFiles[file];
            //When a dependent type is fired, there's no need to launch another.
            if(this.dependentTypes.includes(type)&&this.dependentTypes.includes(f.type)){
                if(!f.waitingTypes.includes("unlink")||f.waitingTypes.includes(type))return;
            }
            f.waitingTypes.push(type);
        }
        if(this.dependentTypes.indexOf(type)!==-1){ //Maybe we will have to wait until a dependency of this file (a parent directory) has been uploaded
            var waiting=this.isProcessingLocalFile(file).then(r=>r.promise||r);
        }
        let fn=this.triggerLocal.bind(this);
        let pr=typeof f!=="undefined"&&f.then?f.then(()=>fn(type,file,stat,waiting)):fn(type,file,stat,waiting); //Await f before calling trigger function
        pr.type=type;
        pr.waitingTypes=[];
        this.localFiles[file]=pr;
        return pr;
    }

    /**
     * Manage local events. This manages a queue
     * @async
     * @param {string} type - Event type
     * @param {string} file - File path
     * @param {Object} [stat={}] stat - File stat
     * @param {(Promise|boolean)} waiting - Promise to wait from progress or false
     * @see _onLocal
     * @returns {Promise<any>} Fulfills when the event has been terminated
     */
    async triggerLocal(type,file,stat={},waiting=false){ //eslint-disable-line max-lines-per-function,max-params
        stat.fileId=await this.local.getxattr(file,"fileId")
            .catch(e=>{
                if(e.code==="ENOENT")return false;
                if(!e.stack)e.stack=new Error().stack;
                throw e;
            })||
                this.remote.hierarchy.get(file,"path").id||
                false;
        var {fileId}=stat;
        //If a file is moved but unlink event is fired before move completed should get fileId from stale tree
        let rfile;
        if(stat.fileId)rfile=this.remote.hierarchy.get(stat.fileId);
        if(rfile&&stat.fileId){
            switch(type){
                case "unlinkDir":
                    await new Promise(r=>setTimeout(r,500)); //Wait 500 ms for add event to appear. Directories trigger unlink before add
                    //falls through
                case "unlink":
                    if(this.moving.includes(stat.fileId))return true; //It's being moved, so event should be discarded
                    break;
                case "add":
                case "addDir":
                    this.moving.push(stat.fileId);
                    type="move"; //Set the type move
                    stat.oldPath=rfile.path;
                    break;
                default:
            }
        }else if(stat.fileId&&!rfile){ //If file has an ID associated but there's no entry in conversion tree, ID is not necessary and can cause bugs
            Reflect.deleteProperty(stat,"fileId");
            this.local.removexattr(file,"fileId").catch(e=>{
                if(e.code==="ENOENT")return;
                throw e;
            });
        }
        let fn=this._onLocal.bind(this);
        let item={
            fn,
            args: [type,file,stat],
            mode: "upload",
            type,
            path: file
        };
        let progress=new Promise((resolve,reject)=>{
            item.callback=resolve;
            item.callbackError=reject;
            if(waiting&&waiting.then){
                waiting.then(()=>{
                    this.queue.push(item);
                });
            }else{
                this.queue.push(item);
            }
        });
        progress.finally(()=>{
            Reflect.deleteProperty(this.localFiles,file);
            if(type==="move"){
                let index=this.moving.indexOf(fileId);
                if(index>=0)this.moving.splice(index,1);
            }
        });
        progress.catch(e=>{
            if(e.code=="ENOENT")return; //Keep ENOENT errors in this scope
            throw e;
        });
        return await progress;
    }

    /**
     * Local events handler
     * @param {string} type - Event type
     * @param {string} file - File path
     * @param {object} stat - File stat
     * @returns {Promise<any>} Fulfills when the upload is done
     */
    //Another function allowed to be complex
    //eslint-disable-next-line complexity
    async _onLocal(type,file,stat){ //eslint-disable-line max-lines-per-function
        let retryValues=["retry","metadata"];
        console.log(`Local event ${type} ${file}`);
        if(stat)var {fileId,oldPath}=stat; //Declarations jump to function scope, so they're declared but undefined if not stat
        this.remoteBlacklistFile(file);
        if(oldPath)this.remoteBlacklistFile(oldPath);
        var f={
            path: file,
            fileId
        }
        var files,dirs;
        switch(type){
            case "move":
                var skipBody=true; //When a file is moved, only metadata has to be uploaded
                //falls through
            case "add":
            case "change":
                if(!await this.local.exists(f.path))return false;
                f.fd=await this.local.openFile(f.path);
                if(!f.fd)return false;
                files=[f];
                //falls through
            case "addDir":
                if(!files)dirs=[f];
                if(!f.fd&&!await this.local.exists(f.path))return false; //File deleted before this function was picked by worker
                var retry=false;
                var retryCount=0;
                do{
                    if(retry||!stat||!stat.mtime){ //mtime is mandatory
                        let arg=f.fd||file;
                        let fn=f.fd?'fstatAsync':'statAsync';
                        stat=await this.local[fn](arg);
                        if(retry!=="metadata"&&f.fd){ //Metadata update doesn't require another fd
                            await this.local.closeFile(f.fd);
                            f.fd=await this.local.openFile(f.path); //Reopen file (cannot reuse fd from readStream)
                        }
                    }
                    if(stat){
                        f.ctime=stat.ctime;
                        f.atime=stat.atime;
                        f.mtime=stat.mtime;
                        if(stat.mimeType)f.mimeType=stat.mimeType;
                    }
                    retry=await this.remote.uploadList({files,dirs},skipBody||retry=="metadata"?true:null)
                        .then(()=>this.validateUpload(f))
                        .catch(error=>this.catcher("local",{type,file:f,error},retryCount)); //eslint-disable-line no-loop-func
                    retryCount++; //eslint-disable-line no-plusplus
                }while(retryValues.includes(retry)&&!this.stop);
                if(f.fd)this.local.closeFile(f.fd);
                break; //Dont't return. This will add extended attributes
            case "unlink":
            case "unlinkDir": //Directories are managed as files by Drive
                return this.remote.deleteFile(f)
                    .catch(error=>this.catcher("local",{type,file:f,error}))
                    .finally(()=>this.remoteBlacklistFile(file,true));
            default:
                throw new Error(`Unknown event ${type} ${file}`);
        }
        await this.local.setxattr(file,"fileId",fileId||this.remote.hierarchy.get(file,"path").id).catch(e=>{
            if(e.code==="ENOENT")return;
            if(!e.stack)e.stack=new Error().stack;
            throw e;
        });
        await new Promise(r=>setTimeout(r,500));
        return this.remoteBlacklistFile(file,true);
    }

    /**
     * Validate a file upload
     * @async
     * @param {Object} file - File resource
     * @param {string} file.path - Relative file path
     * @param {number=} file.fd - File descriptor (optional)
     * @returns {Promise<(boolean|string)>} A string as "retry" if the complete transference must be retried "metadata" on incorrect date. False otherwise
     */
    async validateUpload(file){
        if(isDirectory(file))return false;
        let props=this.remote.hierarchy.get(file.path,"path");
        let stat;
        if(file.fd){
            stat=await this.local.fstatAsync(file.fd);
        }else{
            stat=await this.local.statAsync(file.path);
        }
        if(!stat)return false; //File not exists
        if(props){
            if(typeof props.size!="undefined"){
                if(Number(props.size)!=Number(stat.size))return "retry";
            }
            if(typeof props.modifiedTime!="undefined"){
                let dt=Number(new Date(props.modifiedTime)); //Remote date
                let mtime=Number(stat.mtime); //Local date
                if(mtime>dt)return "retry"; //When the local file is newer, a whole file upload is needed.
                if(dt!=mtime)return "metadata";
            }
        }
        return false;
    }

    /**
     * Catch errors from events
     * @param {string} origin - Error origin
     * @param {Object} details - Error details
     * @param {string} details.type - Event type where the issue came from
     * @param {string} details.file - File path
     * @param {Error} details.error - Error thrown
     * @param {number} [retryCount=0] - Upload retried n times
     * @returns {undefined} void
     */
    async catcher(origin,details,retryCount=0){
        var {type,file,error}=details;
        if(error.fatal)throw error;
        if(origin=="local"){
            if(error.code===404){ //There is a problem with the conversion tree
                if(retryCount>=(this.config.maxRetries||5)){ //A file can be retried 5 times (by default) at max before throwing an error
                    if(file.fd)this.local.closeFile(file.fd).catch(e=>console.warn(e.message));
                    throw error;
                }
                return this.remote.updateTree().then(()=>"retry"); //A retry status in order to reexecute transaction
            }
            if(error.code==="ENOENT"||error.code==="EBADF")return console.warn(error.message||"File deleted before uploaded");
        }
        return console.error(`Error executing ${origin} ${type} on ${file.path} `,error.message||error);
    }

    /**
     * Blacklist a local file
     * @param {string} path - File path
     * @param {boolean=} reverse - True to remove from blacklist
     * @see _blacklistFile
     * @returns {boolean} Return value from function
     */
    blacklistFile(path,reverse){
        return this._blacklistFile(path,this.blacklist,reverse);
    }

    /**
     * Blacklist a remote file
     * @param {string} path - File path
     * @param {boolean=} reverse - True to remove from blacklist
     * @see _blacklistFile
     * @returns {boolean} Return value from function
     */
    remoteBlacklistFile(path,reverse){
        return this._blacklistFile(path,this.remoteBlacklist,reverse);
    }

    /**
     * Add a file to a given blacklist
     * @param {string} path - File path
     * @param {Array} list - List to modify
     * @param {boolean=} reverse - Remove from list instead of adding
     * @returns {boolean} True if modified correctly
     */
    _blacklistFile(path,list,reverse){
        if(reverse){
            let item=list.filter(f=>f==path)[0];
            if(!item)return true; //That path is not blacklisted
            let index=list.indexOf(item);
            if(list.splice(index,1))return true;
            return false;
        }
        if(this.isBlacklisted(path))return true; //It's backlisted
        list.push(path);
        return true;
    }

    /**
     * Check if a file is in the local file blacklist
     * @param {string} path - Path to check
     * @see _isBlacklisted
     * @returns {boolean} Is the file in the blacklist?
     */
    isBlacklisted(path){
        return this._isBlacklisted(path,this.blacklist);
    }

    /**
     * Check if a file is in the remote file blacklist
     * @param {string} path - Path to check
     * @see _isBlacklisted
     * @returns {boolean} Is the file on the remote blacklist?
     */
    isRemoteBlacklisted(path){
        return this._isBlacklisted(path,this.remoteBlacklist);
    }

    /**
     * Check if a file is in a given blacklist
     * @param {string} path - The file path
     * @param {Array} list - The list to check against
     * @returns {boolean} Is the file in the given blacklist?
     */
    _isBlacklisted(path,list){
        return list.filter(f=>f==path).length>0;
    }

    /**
     * Check if a file (or a parent path) is in the processing list
     * @async
     * @param {string} path - File path to test
     * @param {boolean} secondCall - Prevents a infinite bucle on unreachable elements
     * @returns {Object} Promise if found, false when not found.
     */
    async isProcessingLocalFile(path,secondCall=false){
        if(this.remote.hierarchy.parentCreated(path)===true)return false; //Don't wait. Parent exists
        let parent=`/${path.split("/").filter(p=>p!="")
            .slice(0,-1)
            .join("/")}`;
        let sel=this.localFiles[parent]; //Get parent from files on the creation queue
        if(sel)return {promise:sel}; //So return Promise from file
        if(!secondCall){
            await new Promise(r=>setTimeout(r,path.split("/").length*100)); //Inner elements wait more for parents than outer ones
            let rtaaw=this.isProcessingLocalFile(path,true);
            if(rtaaw)return rtaaw; //Object created in function
        }
        let res=[]; //Should not reach this point as the parent must exist or be in the processing queue. If none of the above is true, this cannot work, but serializing requests can be helpful
        for(let i of Object.keys(this.localFiles)){
            let cmp=this.isOnTheSamePath(path,i);
            if(cmp)res.push(this.localFiles[i]);
        }
        return res?{promise:Promise.all(res)}:false; //False if not in processing queue
    }

    /**
     * Check if path1 and path2 have the same origin path
     * @param {string} path1 - First path yo check
     * @param {string} path2 - Second path to check
     * @returns {boolean} True or false id the root path is the same or not
     */
    isOnTheSamePath(path1,path2){
        let p1=path1.split("/").filter(r=>r)[0];
        let p2=path2.split("/").filter(r=>r)[0];
        return p1===p2;
    }

    /**
     * Get the stop status
     * @returns {(boolean|undefined)} Stop status (true or undefined)
     */
    get stop(){
        return this._stop;
    }

    /**
     * Set the stop flag to value
     * @param {boolean} value - A boolean or something evaluated as a boolean
     * @returns {undefined} void
     */
    set stop(value){
        this._stop=value;
        this.queue.stop=value;
        if(this.sock)this.sock.stop();
    }

    bindSocket(socketConfig={id:global.JDriveID}){
        if(socketConfig.disable)return;
        this.sock=new Socket(socketConfig,this.queue);
    }
}

/**
 * HOW BLACKLISTS WORK:
 * Local blacklist
 * Methods: isBlacklisted, blacklistFile
 * Called from Remote event handler when working with a local file.
 * Checked from Local event handler when a event is fired from the local filesystem
 *
 * Remote blacklist
 * Methods: isRemoteBlacklisted, remoteBlacklistFile
 * Called from the Local event handler when working with a remote file.
 * Checked from Remote event handler when a event is fired from Drive
 */

module.exports={
    Events
}
