const fs=require("fs-extra");
const chokidar=require("chokidar");
const klaw=require("klaw-sync");
const path=require("path");
const {isDirectory}=require("./drive");
const utime=require("./utime");
const mime=require("mime-types");
const {Xattrs}=require("./xattrs");

/**
 * Local filesystem utilities
 * @module lib/localfs
 */

/**
 * The class itself
 * @typedef {Object} this
 * @see Watch
 */

/**
 * A readable stream. See nodejs docs about this
 * @typedef ReadableStream
 */

/**
 * A writable stream to a file or fd. See nodejs docs about this
 * @typedef WritableStream
 */

/**Filesystem class*/
class Watch{

    /**
     * @param {string} dir - Directory to watch
     * @param {Object} config - Configuration object. Also includes ignore config
     */
    constructor(dir,config={}){
        this.dir=dir;
        this.hooks={};
        this.mode="600";
        this.dirMode="700";
        if(config.modes){
            this.mode=config.modes.file;
            this.dirMode=config.modes.dir;
        }
        if(config.ignored){
            if(typeof config.ignored=="object"){
                this.ignored=[];
                for(let i in config.ignored){
                    if(!config.ignored.hasOwnProperty(i))continue;
                    let reg=new RegExp(config.ignored[i]);
                    this.ignored.push(reg);
                }
            }else{
                this.ignored=new RegExp(config.ignored);
            }
        }
        this.xattrs=new Xattrs(this.dir,config);
        this.lock=this.init();
    }

    /**
     * Add the tree to class
     * @async
     * @returns {undefined} void
     */
    async init(){
        this.xattrs.lock.catch(()=>{
            console.error(this.error||"eXtended Attributes cannot be used. Some bugs may appear when moving shared files");
        });
        this.tree=this.genTree();
        await this.xattrs.lock;
    }

    /**
     * Start watching this.dir
     * @async
     * @returns {Promise<undefined>} Void
     */
    async startWatching(){
        this.watcher=chokidar.watch(this.dir,{
            ignored: this.ignored
        });
        this.watcher.on("error", e=>{
            if(e.code=="EACCES")return console.error("Cannot access ",e.path);
            throw e;
        });
        await new Promise(resolve=>{
            this.watcher.on("ready",resolve);
        });
        this.setEvents();
    }

    /**
     * Set events to listen for
     * @param {string[]} ev - Events array
     * @returns {undefined} void
     */
    setEvents(ev){
        let eventsFallback=["add", "addDir", "change", "unlink", "unlinkDir"];
        let events=ev||eventsFallback;
        this.on("all", (...args)=>this.updateFile(...args));
        events.forEach(event=>{
            this.watcher.on(event, (f,s)=>this.fire(event,f,s));
        });
    }

    /**
     * Add file to watcher
     * @param {string} d - Path to watch
     * @returns {any} Not reusable
     */
    add(d){
        console.log(`Adding ${d} to watched dirs`);
        return this.watcher.add(d);
    }

    /**
     * Add a event listener to localfs
     * @param {string} event - Event to listen for
     * @param {Function} callback - Function to call on event
     * @returns {this} The self class. Chaining is possible
     */
    on(event,callback){
        if(this.hooks[event]){
            this.hooks[event].push(callback);
        }else{
            this.hooks[event]=[callback];
        }
        return this;
    }

    /**
     * Fire a event and call the listeners
     * @param {string} event - Event to emit
     * @param {string} f - File path
     * @param {Object} s - File stat (if any)
     * @returns {undefined} void
     */
    fire(event,f,s){
        let file=path.resolve(`/${f.split(this.dir)[1]}`);
        if(typeof s=="object"&&!s.mimeType)s.mimeType=mime.lookup(file);
        if(this.hooks.all){
            this.hooks.all.forEach(hook=>hook(event,file,s));
        }
        if(event=="all")return;
        if(this.hooks[event]){
            this.hooks[event].forEach(cb=>cb(file,s));
        }
    }

    /**
     * Generate the local tree
     * @returns {Array} Array of files in the this.dir tree
     */
    genTree(){
        let t=klaw(this.dir);
        return t.map(s=>this.genFile(s));
    }

    /**
     * Generate a file from a given path
     * @param {Object} resource - The resource to convert
     * @param {Object} resource.stats - Stats object for the file
     * @param {string} resource.path - Absolute file path
     * @returns {Object} Generated resource
     */
    genFile(resource){
        let {stats}=resource;
        stats.mimeType=mime.lookup(resource.path);
        return {
            stats,
            isDirectory: stats.isDirectory(),
            path:path.resolve(`/${resource.path.split(this.dir)[1]}`) //Split the path and save only the relative path from Sync directory
        };
    }

    /**
     * Handle call to ensure dir
     * @param {string} a - File path
     * @returns {Promise<any>} Not usable
     */
    ensure(a){
        return fs.ensureDir(path.join(this.dir,a));
    }

    /**
     * Handle call to remove
     * @async
     * @param {string} a - Path relative to this.dir
     * @returns {Promise<any>} Not usable
     */
    async remove(a){
        let p=path.join(this.dir,a);
        return await fs.remove(p);
    }

    /**
     * Move a file
     * @async
     * @param {Object} a - Two files
     * @param {string} a.old - Old file to move
     * @param {string} a.file - Destination path
     * @returns {Promise<any>} Not usable
     */
    async move(a){
        let {old,file}=a;
        if(!old.path||!file.path)throw new Error(`Cannot get property path from ${!old.path?'old file':'new file'}`);
        let oldpath=path.join(this.dir,old.path);
        let filepath=path.join(this.dir,file.path);
        return await fs.move(oldpath,filepath,{overwrite:true}); //By default, 'mv' overwrites the files on the destination path
    }

    /**
     * Create a write stream
     * @param {string} f - File path to write
     * @returns {WritableStream} A writable stream to the path
     */
    createStream(f){ //A write stream
        let p=path.join(this.dir,f);
        return fs.createWriteStream(p);
    }

    /**
     * Create a read stream
     * @param {string} f - File path to the file to read
     * @param {number} fd - File descriptor
     * @throws ENOENT
     * @returns {ReadableStream} A read stream
     */
    createReadStream(f,fd){
        let p=path.join(this.dir,f);
        let options={
            fd,
            autoClose:typeof fd=="undefined"
        }
        return fs.createReadStream(p,options);
    }

    /**
     * Write a file with contents
     * @param {string} a - File path
     * @param {(string|Buffer)} c - File contents
     * @returns {Promise<any>} Not usable
     */
    writeFile(a,c){
        let p=path.join(this.dir,a);
        return fs.writeFile(p,c);
    }

    /**
     * Set attributes to a file like times
     * @async
     * @param {string} file - File path
     * @param {Object} options - Options object
     * @param {any} options.atime - Date init for access time
     * @param {any} options.mtime - Date init for modification time
     * @param {(string|number)} options.mode - Permissions to the file
     * @throws ENOENT
     * @see lib/utime
     * @returns {Promise<Array>} Fulfills when all tasks are done
     */
    async attr(file,options={}){
        let tasks=[];
        let atime=new Date(options.atime||file.viewedByMeTime||0); //There's no a viewedTime property
        let mtime=new Date(options.mtime||file.modifiedTime||0); //No date=0
        let p=path.join(this.dir,file.path);
        tasks.push(utime(p,{atime,mtime}));
        if(isDirectory(file)){
            tasks.push(fs.chmod(p,options.mode||this.dirMode));
        }else{
            tasks.push(fs.chmod(p,options.mode||this.mode));
        }
        if(file.id)tasks.push(this.setxattr(file.path,"fileId",file.id));
        return await Promise.all(tasks);
    }

    /**
     * Handle xattr set (convert file path)
     * @param {string} file - Relative path
     * @param {string} name - Name for xattr
     * @param {(string|Buffer)} value - Value for xattr
     * @returns {Promise<undefined>} Promise fulfilled when xattr set
     * @see xattrs.js
     */
    setxattr(file,name,value){
        let p=path.join(this.dir,file);
        if(!this.xattrs.usable)return Promise.resolve();
        return this.xattrs.setXattr(p,name,value);
    }

    /**
     * Handle xattr get (convert file path)
     * @param {string} file - Relative path
     * @param {string} name - Name of xattr
     * @returns {Promise<(string|undefined)>} Promise fulfilled when xattr value ready
     * @see xattrs.js
     */
    getxattr(file,name){
        let p=path.join(this.dir,file);
        if(!this.xattrs.usable)return Promise.resolve(); //Try to avoid an error
        return this.xattrs.getXattr(p,name);
    }

    /**
     * Handle xattr remove (convert file path)
     * @param {string} file - Relative path
     * @param {string} name - Xattr name
     * @returns {Promise<undefined>} Promise fulfilled when xattr has been removed
     * @see xattrs.js
     */
    removexattr(file,name){
        let p=path.join(this.dir,file);
        if(!this.xattrs.usable)return Promise.resolve();
        return this.xattrs.removeXattr(p,name);
    }

    /**
     * Check if a path exists
     * @param {string} a - File path
     * @returns {Promise<boolean>} Depending if the path exists
     */
    exists(a){
        let p=path.join(this.dir,a);
        return fs.pathExists(p);
    }

    /**
     * Update the local filesystem tree
     * @returns {undefined} void
     */
    updateTree(){
        this.tree=this.genTree();
    }

    /**
     * Update a file's metadata when an event is fired
     * @param {string} type - One of the types thrown by chokidar
     * @param {string} file - Path where the modified file is
     * @param {Object} stats - Stat object from fs.stat or chokidar.
     * @returns {undefined} Void
     */
    updateFile(type,file,stats){
        let mod=["add","addDir","change"]
        let del=["unlink","unlinkDir"];
        let prop,index;
        prop=this.tree.filter(p=>p.path==file);
        if(prop)index=this.tree.indexOf(prop);
        if(mod.includes(type)){
            if(index>=0)this.tree[index]=this.genFile({path:file,stats});
        }else if(del.includes(type)){
            if(index>=0)this.tree.splice(index,1);
        }
    }

    /**
     * Get the stop status
     * @returns {boolean} this._stop
     */
    get stop(){
        return this._stop;
    }

    /**
     * If val is true stop watching
     * @param {boolean} val - Stop status
     * @returns {string} Always a happy face :)
     */
    set stop(val){
        this._stop=val;
        if(val){
            if(this.watcher)this.watcher.close();
        }
        return ":)";
    }

    /**
     * Get the stat of a file (sync)
     * @param {string} file - File path
     * @param {boolean} bypassCache - Bypass the cache tree and do the syscall
     * @returns {Object} File stat (from the tree or an actual stat)
     */
    stat(file,bypassCache){
        if(!bypassCache){
            let res=this.tree.filter(f=>f.path===file);
            if(res.length>0)return res[0].stats;
        }
        let p=path.join(this.dir,file);
        return fs.statSync(p);
    }

    /**
     * Get stat of a file (async)
     * @async
     * @param {string} file - File path
     * @returns {Promise<(Object|boolean)>} Promise with the file stat or false if file not exist
     */
    async statAsync(file){
        let exists=await this.exists(file);
        if(!exists)return exists;
        let p=path.join(this.dir,file);
        let st=await fs.stat(p);
        this.updateFile("change",file,st);
        return st;
    }

    /**
     * Get stat from fd instead of path
     * @async
     * @param {number} fd - File descriptor
     * @returns {Promise<Object>} Stat object
     */
    async fstatAsync(fd){
        return await fs.fstat(fd);
    }

    /**
     * Open a file (defaults to read-only)
     * @param {string} file - Relative file path
     * @param {number} [mode=0o666] - Opening mode
     * @returns {Promise<number>} Any given file descriptor
     */
    openFile(file,mode=0o600){
        let p=path.join(this.dir,file);
        return fs.open(p,mode);
    }

    /**
     * Close a File Descriptor
     * @param {number} fd - File descriptor to close
     * @returns {Promise<undefined>} Promise fulfilled when fd is closed
     */
    closeFile(fd){
        return fs.close(fd);
    }
}

module.exports={
    Watch
};
