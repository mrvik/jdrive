const fs=require("fs-extra");

/**
 * Module providing a interface to a lockfile
 * @module lib/lockfile
 */

/**
 * A lockfile template
 * @readonly
 * @const {object}
 */
var template={
    status: "first-run",
    lockfile:{
        date: Date.now()
    },
    drive:{}
}

/**Provides a lockfile control*/
class Lockfile{

    /**
     * @param {string} file - Lockfile absolute path
     */
    constructor(file){ //To avoid unexpected behaviour, please, use absolute paths
        if(!file)throw new Error("No lockfile specified");
        this.filename=file;
        this.lock=this.init();
        this.lock.then(()=>{
            if(!this.contents.lockfile)this.contents.lockfile=template.lockfile;
            this.updateLock();
        });
    }

    /**
     * Starts reading the lockfile or writing it
     * @async
     * @throws EACCESS
     * @returns {Promise<boolean>} True always
     */
    async init(){
        this.contents=await fs.readFile(this.filename,"UTF-8")
            .then(s=>{
                return JSON.parse(s);
            })
            .catch(e=>{
                if(e.fatal)throw e;
                return this.generateLock();
            });
        return true;
    }

    /**
     * Generate a lockfile from template
     * @async
     * @see template
     * @returns {Promise<Object>} The lockfile contents. From template
     */
    async generateLock(){
        let lock=template;
        return await this.saveLock(lock).then(()=>lock);
    }

    /**
     * Flush contents of lockfile. Don't call it directly!
     * @async
     * @param {Object} [content=this.contents] - Contents of lockfile. Stringified before writing
     * @throws EACCESS
     * @returns {Promise<any>} Fulfills when file is flushed
     */
    async saveLock(content){
        let sav=JSON.stringify(content||this.contents);
        return fs.writeFile(this.filename,sav,"UTF-8");
    }

    /**
     * Update the lockfile with current values
     * @async
     * @see saveLock
     * @returns {Promise<any>} Fulfills when file is flushed
     */
    async updateLock(){
        await this.lock;
        this.contents.lockfile.date=Date.now(); // Also save the date in the lockfile
        this.lock=this.saveLock();
        return await this.lock;
    }

    /**
     * Updates lockfile synchronously. Only use this as last resort to prevent corruption
     * @param {Object} [content=this.contents] - Contents of the lockfile. Stringified before writing
     * @returns {any} Not usable
     */
    updateLockSync(content){
        let sav=JSON.stringify(content||this.contents);
        return fs.writeFileSync(this.filename,sav,"UTF-8");
    }
}

module.exports=Lockfile;
