/**
 * @author Víctor González (MrViK) <mrvikxd@gmail.com>
 * @module lib/queue
 */

/**
 * A ES6 generator function
 * @typedef {Function} Generator
 */

/** Class which creates a queue iterable by an arbitrary number of workers*/
class Queue{

    /**
     * Generate the class
     * @param {number} workers Max number of concurrent workers. Usually 2 are sufficient. 1 is considered stable, but multiple ones can speed up transfers
     */
    constructor(workers=2){
        this._queue=[];
        this.now=[];
        if(workers<1)throw new Error("There should be at least one worker");
        if(workers>8)console.warn(`Warning: ${workers} workers will be spawned. This can lead to missing files in uploads due to Drive API ratelimiting`);
        this.workers=this.createWorkers(workers);
        this.stop=false;
    }

    get queue(){
        return this._queue;
    }

    set queue(item){ //Some other way of pushing tasks
        return this._queue.push(item); //Avoid using this
    }

    /**
     * @param {Object} item The item to push. It must contain fn, callback and args array (spreaded)
     * @returns {Object} The same item on param
     */
    async push(item){
        item.working=typeof item.working!="undefined"?item.working:false;
        this._queue.push(item);
        return item;
    }

    /**
     * @param {number} max Max workers to be spawned
     * @returns {Array} Exposes the created workers. A return value if workerKeepruning
     */
    createWorkers(max){
        let rt=[];
        for(let i=0; i<max; i++){
            rt[i]=this.workerKeepruning(this.worker);
        }
        return rt;
    }

    /**
     * @param {Generator} worker A iterable generator named worker
     * @returns {Promise<undefined>} A promise fulfilled when the worker is stopped.
     */
    async workerKeepruning(worker){
        let bound=worker.bind(this);
        for(let rt of bound()){
            await rt;
            if(this.stop)break;
            if(this.queue.length<1)await new Promise(r=>setTimeout(r,2000)); //Wait 2 seconds if Worker didn't do anything
        }
    }

    /**
     * @generator
     * @yields {Promise<any>} A promise containing the result of called function or true
     * @returns {undefined} When the worker is stopped, it returns undefined
     */
    * worker(){
        while(true){
            if(this.stop)return;
            if(this._queue[0]){
                let item=this._queue.shift();
                item.working=true;
                this.now.push(item);
                let {fn,args,callback,callbackError}=item;
                let rt=fn(...args)
                    .then(callback);
                if(callbackError)rt.catch(callbackError);
                rt.finally(()=>{
                    let index=this.now.indexOf(item);
                    if(index>=0)return this.now.splice(index,1);
                });
                yield rt;
                //This assumes that the provided function is async, otherwise will fail as fn.then not a function.
            }else{
                yield;
            }
        }
    }
}
module.exports=Queue;
