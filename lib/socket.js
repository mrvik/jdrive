/* eslint-disable max-classes-per-file */
/* eslint no-console:["error", {allow: ["error"]}] */
const {Worker, isMainThread, parentPort, workerData}=require("worker_threads");
const net=require("net");

/**
 * @typedef {Object} Queue
 * @property {Object[]} queue - An array of events to be processed
 */

/**
 * @typedef {Object} Stream
 * @external Stream
 * @see {@link https://nodejs.org/api/stream.html}
 */

class SocketBackend{

    /**
     * Create a socket backend
     * @param {Object} config Socket and socket backend configuration
     * @param {Queue} queue Queue object
     */
    constructor(config, queue){
        this.config=config;
        this.queue=queue;
        this.worker=new Worker(__filename,{
            workerData: {config}
        });
        this.createEvents();
    }

    /**
     * Get events from worker
     * @returns {undefined} Void
     */
    createEvents(){
        this.worker.on("message", args=>this.msgHandler(...args));
    }

    /**
     * Receives and replies to messages from Worker
     * @param {number} id Request id to respond (relative to socket queries)
     * @param {string} query Query string
     * @returns {any} Anything returned from sendReply
     */
    msgHandler(id, query){
        if(!id)return console.error("Cannot process a query without id");
        if(!query){
            this.sendReply(id,new Error("Query cannot be undefined!"));
        }
        switch(query){
            case "uploads": {
                let rt=this.uploads;
                return this.sendReply(id,rt);
            }
            case "downloads": {
                let rt=this.downloads;
                return this.sendReply(id,rt);
            }
            case "all": {
                let {uploads,downloads}=this;
                return this.sendReply(id,{uploads,downloads});
            }
            case "workers": {
                return this.sendReply(id, this.workerNumber);
            }
            default:
                this.sendReply(id, new Error("Cannot handle unknown query: "+query));
        }
    }

    /**
     * Send reply to Worker
     * @param {number} id Request id to reply (got from socket at connection time)
     * @param {Object|Error} data Response object
     * @returns {any} Returned from port.postMessage
     */
    sendReply(id, data){
        if(data instanceof Error)return this.worker.postMessage({id,e:data.toString()});
        try{
            return this.worker.postMessage({id,v:data});
        }catch(e){
            return this.worker.postMessage({id,e:e.toString()});
        }
    }

    /**
     * Query a JDrive queue (the one passed to constructor)
     * @param {string} mode - File mode
     * @returns {Object[]} An array of objects with the info of files being processed
     */
    queryQueue(mode){
        return this.queue.queue
            .concat(this.queue.now)
            .filter(item=>item.mode==mode)
            .map(item=>{
                let {type,working,path}=item;
                return {type,working,path};
            });
    }

    /**
     * Get uploads from queue (using queryQueue)
     * @returns {Object[]} Array of files being uploaded with info about them
     */
    get uploads(){
        return this.queryQueue("upload");
    }

    /**
     * Get downloads from queue (using queryQueue)
     * @returns {Object[]} Array of files being downloaded with info about them
     */
    get downloads(){
        return this.queryQueue("download");
    }

    /**
     * Get number of workers on the queue
     * @returns {number} Number of workers running on the queue
     */
    get workerNumber(){
        return this.queue.workers.length;
    }

    /**
     * Stop the Worker
     * @returns {any} From sendReply
     * @see sendReply
     */
    stop(){
        this.sendReply(null,"exit");
    }
}

class WorkerClass{

    /**
     * Create the main object for the worker
     * @param {Object} wmsg Data passed for worker
     * @param {Object} wmsg.config Worker config
     */
    constructor(wmsg){
        this.clients=[];
        this.listeners=[];
        let {config}=wmsg;
        this.config=config;
        this.createEvents();
        this.createSocket(this.path);
    }

    /**
     * Get socket path based on the config and environment
     * @returns {string} Absolute path
     */
    get path(){
        let config=this.config||{};
        if(config.path)return config.path;
        let addon, base;
        if(process.env.XDG_RUNTIME_DIR){
            base=process.env.XDG_RUNTIME_DIR;
        }else{
            base="/tmp";
        }
        if(config.id)addon=config.id;
        else if(process.env.XDG_SESSION_ID)addon=process.env.XDG_SESSION_ID;
        else addon=Math.trunc(Math.random()*1000);
        return `${base}/jdrive-${addon}.sock`;
    }

    /**
     * Start listening for events on parentPort
     * @returns {undefined} void
     */
    createEvents(){
        parentPort.on("message", msg=>this.processMessage(msg));
    }

    /**
     * Create UNIX socket on path
     * @param {string} path Path to place the UNIX socket
     * @returns {undefined} void
     */
    createSocket(path){
        this.server=net.createServer(stream=>{
            this.clients.push(stream);
            stream.on("data", data=>this.receiver(data,stream));
            stream.on("error", e=>{
                console.error(`Error on Socket: ${e}`);
            });
            stream.on("close", ()=>this.clients.splice(this.clients.indexOf(stream), 1));
        })
            .listen(path)
            .on("connection", stream=>{
                stream.write(JSON.stringify({id: this.config.id}));
            });
    }

    /**
     * Receiver function can manage a single query. Passes data to the listening part on the main thread and passes the response to stream
     * @param {string} data Data from socket (usually a command name)
     * @param {Stream} stream Socket stream to write responses into
     * @returns {undefined} void
     */
    receiver(data, stream){
        let id=Math.trunc(Math.random()*10000); //10000 is the max
        let pr=new Promise(r=>{
            this.listeners[id]=r;
        });
        data=data.toString().replace("\n","");
        parentPort.postMessage([id,data]);
        pr.then(res=>stream.write(res)); //data is passed to the stream as-is, so, it should be formatted
    }

    /**
     * Process messages from main thread
     * @param {Object} msg Message from main thread
     * @returns {undefined} void
     */
    processMessage(msg){
        let data=msg.v;
        let error=msg.e;
        let {id}=msg;
        if(data=="exit")return this.cleanup();
        if(error)error={error: true, msg: error};
        data={error: false, data};
        if(this.listeners[id])this.listeners[id](JSON.stringify(error||data));
        Reflect.deleteProperty(this.listeners, id);
    }

    /**
     * Close connection with socket clients and close parent port. This leads the worker thread to stop
     * @returns {undefined} void
     */
    cleanup(){
        this.clients.forEach(client=>client.destroy("JDrive is stopping..."));
        this.server.close();
        parentPort.close();
    }
}

if(isMainThread)module.exports=SocketBackend;
else {
    new WorkerClass(workerData); //eslint-disable-line no-new
}
