const exec=require("util").promisify(require("child_process").exec);
const path=require("path");
var ffi,ref,Struct,ArrayType,ffiError;
try{
    /*eslint-disable global-require*/
    ffi=require("ffi-napi");
    ref=require("ref-napi");
    Struct=require("ref-struct-di")(ref);
    ArrayType=require("ref-array-di")(ref);
    /*eslint-enable global-require*/
}catch(e){
    ffiError=e;
}

/**
 * Provides a interface to utimes call
 * @module lib/utime
 */

/**
 * Time spec as defined on libc
 * @typedef {Object} Timeval
 * @property {number} tv_sec - Seconds
 * @property {number} tv_nsec - Nanoseconds
 */

/** Provides a nanosecond time change interface */
class Timedate{
    constructor(){
        this.lock=this.init();
    }

    /**
     * Sets the internal function to change time (binding or child_process)
     * @async
     * @returns {Promise<undefined>} A Promise which fullfills then the init is done
     */
    async init(){
        try{
            if(typeof ffiError!="undefined")throw new Error(ffiError);
            var time_t=ref.types.long; //eslint-disable-line camelcase
            var long=ref.types.long;
            this.UTIME_OMIT=1073741822; //It's a constant from sys/stat.h
            this.fd=-100; //Another constant from fcntl.h
            this.Timeval=new Struct({
                tv_sec: time_t,
                tv_nsec: long
            });
            this.stringPointer=ref.refType(ref.types.char);
            this.TimevalArray=new ArrayType(this.Timeval);
            this.native=new ffi.Library(null,{
                utimensat:["int",["int",this.stringPointer,this.TimevalArray,"int"]]
            });
        }catch(e){
            this.native=false;
            console.error(e);
            console.error("Native binding not usable, falling back to command execution");
        }
    }

    /**
     * Set the utime of a file (from path)
     * @async
     * @param {string} fpath - Path to file. Will be resolved with path.resolve
     * @param {Object} dates - Dates object with atime and mtime.
     * @param {(Date|number|string)} dates.atime - Atime as Date object or init for Date constructor
     * @param {(Date|number|string)} dates.mtime - Mtime as Date object or init for Date constructor
     * @returns {Promise<any>} Fullfilled promise when the times have been updated. 0 When comes from binding or array from command execution
     */
    async time(fpath,dates){
        await this.lock;
        let p=path.resolve(fpath);
        var atime=new Date(dates.atime);
        var mtime=new Date(dates.mtime);
        if(this.native){
            let res=await this.ffiCall(p, Number(atime), Number(mtime));
            return res;
        }
        let tasks=[];
        tasks.push(this.command(p,atime,"atime"));
        tasks.push(this.command(p,mtime,"mtime"));
        return Promise.all(tasks);
    }

    /**
     * Run command touch with arguments
     * @async
     * @param {string} p - Path to file
     * @param {Date} t - Date object
     * @param {string} type - One of atime or mtime. A unknown type will lead to mtime
     * @returns {Object} Process execution result
     */
    async command(p,t,type){
        let arg=type=="atime"?"-a":"-m";
        let cmd=`touch ${arg} --date "${t.toISOString()}" "${p}"`;
        return await exec(cmd);
    }

    /**
     * Call FFI time update
     * @async
     * @param {string} pathname - An absolute path to file
     * @param {number} atime - Atime in milliseconds since epoch
     * @param {number} mtime - Mtime in milliseconds since epoch
     * @throws Error from Glibc applied to utimensat(2)
     * @returns {number} 0 on command OK
     */
    async ffiCall(pathname, atime, mtime){
        await this.lock;
        let times=this.generateArray(atime,mtime);
        let stringP=ref.allocCString(pathname); //Creates a buffer with pathname and a pointer
        let res=this.native.utimensat(this.fd,stringP,times,0);
        let error=ffi.errno(); //Get errno from FFI
        if(res<0)throw new Error(`Failed to set utime ${error}`);
        return res;
    }

    /**
     * Generates an array of timeval from parameters
     * @param {number} atime - Atime in milliseconds since epoch
     * @param {number} mtime - Mtime in milliseconds since epoch
     * @returns {Timeval[]} Array of atime and mtime as Timeval stucts
     */
    generateArray(atime,mtime){
        let array=[];
        if(typeof atime!="undefined"){
            array[0]=this.getTimespec(atime);
        }
        if(typeof mtime!="undefined"){
            array[1]=this.getTimespec(mtime);
        }
        let nativeArray=new this.TimevalArray(array.length);
        for(let i=0; i<array.length; i++){
            nativeArray[i]=new this.Timeval(array[i]);
        }
        return nativeArray;
    }

    /**
     * Create a Timeval from parameter
     * @param {number} seconds - Silliseconds since epoch
     * @returns {Timeval} Timeval struct
     */
    getTimespec(seconds){ //Disable camelcase for this function as those are C identifiers
        /*eslint-disable camelcase*/
        let tv_sec=typeof seconds=="undefined"?this.UTIME_OMIT:Math.trunc(seconds/1000);
        let tv_nsec=typeof seconds=="undefined"?this.UTIME_OMIT:Math.trunc(seconds%1000*1000000);
        return {tv_sec,tv_nsec};
        /*eslint-enable camelcase*/
    }
}
const time=new Timedate();

/**
 * Use Timedate to update a file times concurrently
 * @see Timedate
 * @async
 * @param {string} p - Path to file
 * @param {Object} times - Object with the times to update
 * @param {(Date|number|string)=} times.mtime - File modification time
 * @param {(Date|number|string)=} times.atime - File access time
 * @throws Error when the file is not writable
 * @returns {Promise<any>} Fullfilled promise when times changed OK
 */
async function change(p,times){
    return await time.time(p,times);
}

module.exports=change;
