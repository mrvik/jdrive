const path=require("path");
const assert=require("assert");
const fs=require("fs-extra");
var usable,error,xattr;
try{
    xattr=require("fs-xattr"); //eslint-disable-line global-require
    usable=true;
}catch(e){
    usable=false;
    error=e;
}

/** Provides a xattr interface */
class Xattrs{

    /**
     * Start Xattrs class
     * @param {string=} home - Home for testing
     * @param {Object} [config={}] - Configuration object (from config.local)
     */
    constructor(home,config={}){
        if(usable!==true)throw error;
        this.usable=usable;
        if(config.disableXattrs){
            let err=new Error("Disabled in config");
            this.error=err;
            this.lock=Promise.reject(err);
            this.usable=false;
            return; //Return without testing xattrs (disabled on config)
        }
        this.prefix="user.jdrive-";
        let seed=Math.random()
            .toString()
            .substr(2);
        this.lock=this.test(seed,home).catch(e=>{
            this.usable=false;
            this.error=e;
        });
    }

    /**
     * Set extended attributes
     * @param {string} p - File path (resolved with path.resolve)
     * @param {string} name - Attr name (appended to this.prefix)
     * @param {(string|Buffer)} value - Attr value
     * @returns {Promise<undefined>} Promise resolved when attribute has been set
     */
    setXattr(p,name,value){
        let pathr=path.resolve(p);
        let cname=`${this.prefix}${name}`;
        if(typeof value!=="string"&&!(value instanceof Buffer))return Promise.resolve(); //Don't let other values to get in
        return xattr.set(pathr,cname,value); //xattr.set returns a Promise
    }

    /**
     * Get a extended attribute from file
     * @param {string} p - File path (resolved with path.resolve)
     * @param {string} name - Attr name (appended to this.prefix)
     * @returns {Promise<(string|undefined)>} Promise resolved when xattr is red. String generated from UTF-8 buffer
     */
    getXattr(p,name){
        let pathr=path.resolve(p);
        let cname=`${this.prefix}${name}`;
        return xattr.get(pathr,cname)
            .then(v=>{
                return typeof v.toString=="function"?v.toString("UTF-8"):v;
            })
            .catch(e=>{
                if(e.code==="ENODATA")return;
                e.stack=new Error().stack;
                throw e;
            });
    }

    /**
     * Remove a extended attribute from file
     * @param {string} p - File path (resolved with path.resolve)
     * @param {string} name - Attr name (append to this.prefix)
     * @returns {Promise<undefined>} Promise resolved when xattr is removed
     */
    removeXattr(p,name){
        let pathr=path.resolve(p);
        let cname=`${this.prefix}${name}`;
        return xattr.remove(pathr,cname,);
    }

    /**
     * Runtime test for JDrive
     * @param {string} seed - File seed
     * @param {string=} home - Home for testing
     * @returns {boolean} true when usable
     * @throws {Error} Error when failed to get, set or xattr values not equal
     */
    async test(seed,home){
        let p=path.join(home||path.join(process.env.HOME,".local"),`jdrive-test-${seed}`);
        let err;
        try{
            await fs.ensureFile(p);
            let str=`test-${seed}`;
            await this.setXattr(p,"test",str);
            let r=await this.getXattr(p,"test");
            assert.deepEqual(str,r);
        }catch(e){
            err=e;
        }finally{
            await fs.remove(p);
        }
        if(err)throw err;
        return true;
    }
}

module.exports={
    Xattrs
};

