#!/usr/bin/env node
/**
 * @fileoverview Main script for orchestration of Drive and Local filesystem
 * @author Víctor González (MrViK)
 */

//-------------------------------------
// REQUIREMENTS
//-------------------------------------

const fs = require('fs-extra');
const {Auth}=require("./lib/auth");
const {Drive}=require("./lib/drive");
const {Watch}=require("./lib/localfs");
const Lockfile=require("./lib/lockfile");
const Diff=require("./lib/diff");
const {Events}=require("./lib/events");
const Config=require("./lib/config");
const {ArgumentParser}=require("argparse");
const packageConfig=require("./package.json");


//-------------------------------------
// CONFIGURATION
//-------------------------------------

/**
 * Parses arguments from commandline
 * @type {ArgumentParser}
 */
var parser=new ArgumentParser({
    version: packageConfig.version,
    addHelp: true,
    description: packageConfig.description
});
parser.addArgument(
    ['--config'],{
        help: "Config file (JSON)",
        metavar: "jdrive-config.json"
    }
);
parser.addArgument(
    ['-b','--config-dir'],{
        help: "Base directory for config",
        metavar: "/home/user/.config/jdrive"
    }
)
parser.addArgument(
    ['-c','--credentials'],{
        help: "Custom credentials file (relative to base)",
        metavar: "credentials.json"
    }
);
parser.addArgument(
    ['-t','--token'],{
        help: "Your access token file (relative to base)",
        metavar: "token.json"
    }
);
parser.addArgument(
    ['-l','--lockfile'],{
        help: "JDrive lockfile (relative to base)",
        metavar: "lockfile.json"
    }
);
parser.addArgument(
    ['-i', '--ignore-file'],{
        help: "gitignore-like file (relative to base)",
        metavar: ".jdriveignore"
    }
)
parser.addArgument(
    ['-d', '--local-dir'],{
        help: "Absolute path to sync your Google Drive",
        metavar: "/home/user/GoogleDrive"
    }
)
parser.addArgument(
    ['--auth-only'],{
        help: "Auth and exit",
        action: "storeTrue"
    }
)
parser.addArgument(
    ["--dry-run"],{
        help: "Auth, log changes and exit",
        action: "storeTrue",
        dest: "dryRun"
    }
)
parser.addArgument(
    ["--id"],{
        help: "JDrive instance id"
    }
)

/**
 * Parsed arguments object
 * @const {object}
 */
const args=parser.parseArgs();

/**
 * Class containing configuration
 * @see module:config
 * @readonly
 * @const {Config}
 */
const cfg=new Config({args});

/**
 * Files that will be used by JDrive
 * @readonly
 * @const {object}
 */
const {files}=cfg;

/**
 * Global configuration for JDrive
 * @const {object}
 */
const {config}=cfg;

/**
 * Authentication scopes
 * @readonly
 * @const {string[]}
 */
const {scopes}=cfg;

var drive,watcher,events;


//-------------------------------------
// Global scoped variables
//-------------------------------------

global.isDev=process.env.NODE_ENV?process.env.NODE_ENV.match("^dev"):false;
var lockfile=new Lockfile(files.lock); //The lockfile is shared between scripts
global.lockfile=lockfile;


//-------------------------------------
// UTILITIES
//-------------------------------------

function stopEverything(err){
    console.log("Asking drive and localfs to stop...");
    if(typeof events!="undefined"&&events.queue)events.stop=true;
    if(typeof drive!=="undefined")drive.stop=true;
    if(typeof watcher!=="undefined")watcher.stop=true;
    if(err===true)setTimeout(()=>{
        throw new Error("Stopping due to error");
    },3000);
}

function syncStopped(){
    global.lockfile.contents.status="sync-stopped";
    return global.lockfile.updateLockSync();
}
function parsePerm(mode){
    //eslint-disable-next-line no-bitwise
    return Number('0' + (mode & 0o777).toString(8));
}


//-------------------------------------
// HERE WE GO!
//-------------------------------------

(async function(){
    await cfg.lock;
    async function createDrive(){
        try{
            var creds=JSON.parse(fs.readFileSync(files.credentials));
        }catch(e){
            if(e.code=="ENOENT")throw new Error("You must create a credentials file on "+e.path);
            throw e;
        }
        var token;
        try{
            token=fs.readFileSync(files.token,"UTF-8");
            fs.stat(files.token).then(st=>{
                let others=Number(parsePerm(st.mode).toString()
                    .substr(-2,2));
                if(others>0)console.error("UNSAFE PERMISSION ON TOKEN FILE: ",files.token);
            });
            token=JSON.parse(token);
        }catch(e){
            console.log("Creating a new token");
        }
        let a=new Auth({creds,token,files,scopes});
        await a.lock;
        console.log("API Authentication OK");
        var _drive=new Drive(a.client,{config:config.drive});
        //This call to process.exit is absolutely necessary
        if(cfg.stopAfterAuth)_drive.stop=true;
        return _drive;
    }
    await lockfile.lock;
    let {status}=lockfile.contents;

    await fs.ensureDir(files.localDir); // dir should exist
    drive=await createDrive();
    await drive.lock;
    if(drive.stop)return stopEverything();
    watcher=new Watch(files.localDir,config.local); //Watcher is not started for now...
    drive.registerFS(watcher);
    events=new Events({
        remote: drive,
        local: watcher,
        config: config.events
    });
    drive.on("all",(...a)=>events.onRemote(...a));
    watcher.on("all",(...a)=>events.onLocal(...a));
    config.socket={disable:cfg.dryRun,id:global.JDriveID,...config.socket};
    events.bindSocket(config.socket);
    console.log("Starting from status: ",status);

    var syncExit;
    if(!status)status="first-run";
    switch(status){
        case "first-run":
            //Starting from 0
            if(cfg.dryRun){
                console.log("This is the first run!");
                let log=drive.tree.map(f=>{
                    let {name,path}=f;
                    let origin="remote";
                    return {name,path,origin}
                }).filter(f=>f.path);
                console.table(log);
                return stopEverything();
            }
            lockfile.contents.status="syncing";
            await lockfile.updateLock();
            syncExit=await drive.syncEverything();
            await watcher.lock;
            await watcher.updateTree();
            break;
        case "syncing": {
            let e=new Error("Sync was interrupted, please, verify the local filesystem and change the lockfile status to 'watching'");
            e.fatal=true;
            throw e;
        }
        case "sync-stopped":
        case "watching": {
            //Sync the diff and continue watching...
            lockfile.contents.status="syncing";
            if(!cfg.dryRun)lockfile.updateLock();
            await watcher.lock;
            let lostEvents=await drive.getLostEvents();
            let diffArgs=[{a:drive.hierarchy.equivalent,b:watcher.tree,events:lostEvents},config.local?config.local.ignored:null];
            const d=new Diff(...diffArgs);
            let log=d.REvents
                .map(ev=>{
                    let {name,path,modifiedTime}=ev[1].file;
                    let origin="remote";
                    let event=ev[0];
                    return {name,path,origin,event,modifiedTime}
                })
                .concat(d.LEvents.map(ev=>{
                    let {name,path,stats}=ev[1].file;
                    let origin="local";
                    let event=ev[0];
                    let modifiedTime=typeof stats!=="undefined"?stats.mtime:null;
                    return {name,path,origin,event,modifiedTime};
                }));
            if(log.length>0||cfg.dryRun)console.table(log);
            if(cfg.dryRun)return stopEverything();
            let evH=[];
            d.REvents.forEach(ev=>evH.push(events.onRemote(...ev)));
            d.LEvents.forEach(ev=>evH.push(events.onLocal(ev[0],ev[1].file.path)));
            Promise.all(evH)
                .then(()=>{
                    lockfile.contents.drive.pageToken=drive.startPageToken;
                    return lockfile.updateLock();
                })
                .then(()=>watcher.updateTree());
            break;
        }
        default:
            //What the fsck?
            throw new Error("Unknown latest status "+status);
    }

    if(syncExit===false)return syncStopped();
    lockfile.contents.status="watching";
    await Promise.all([watcher.lock,lockfile.updateLock()]);
    console.log("Starting watchers...");
    await Promise.all([watcher.startWatching(),drive.startWatching()]); //Resume the watch process
})();


//-------------------------------------
// PROCESS HANDLERS
//-------------------------------------

process.on("unhandledRejection",(e,p)=>{
    //Save the world
    if(!e.fatal){ //This is the last resort. Shouldn't get here, but some errors are hard to catch
        let avoidable=["ENOENT","EBADF"];
        if(e.code&&avoidable.includes(e.code)){
            console.warn(global.isDev?e:(e.message||e));
            return;
        }
    }
    if(global.isDev){
        console.log(p);
        console.error(e);
    }else{
        console.error(e.message||e);
    }
    if(!e.fatal&&global.lockfile&&global.lockfile.contents.status=="syncing")syncStopped();
    stopEverything(true);
});
process.on("SIGINT",stopEverything);
process.on("SIGTERM",stopEverything);
