# UNIX Socket docs

## Location
It's located on `/tmp/jdrive-$USER-$ID` where $ID is provided by the `--id` parameter (or a random number from 0 to 1000 if $ID not defined)

## Messages
All the messages are valid JSON.

- Welcome message: On connection, JDrive reports the ID on a JSON as follows:
  - id (string) JDrive instance ID
- Now it's waiting for one (or more) queries. All of them have the following response:
  - error (bool) False when the query is successful
  - msg (string) Defined if error==true
  - data (depends on the query) It's no present when error==true

### Allowed queries

Those commands can have a "\n", so you can query the socket directly with `nc -U`

  - `uploads` - Query files being uploaded (or to be uploaded). Data is as follows:
    - ([]file) - Array of files being processed
  - `downloads` - Same as uploads
  - `all` - Query all queues (uploads and downloads)
    - uploads ([]file) - Array of files being uploaded (or to be uploaded)
    - downloads ([] file) - Array of files being downloaded (or to be downloaded)
  - `workers` - Query number of workers on events queue
    - (int) - Number of workers

## Types

- `file` - Object that represents a file and some properties
  - type (string) - Event type that is being handled
  - path (string) - Complete file path (or file name when processing remote events)
  - working (bool) - Worker has started to process the file

## Leaving the connection

Client can leave connection at any moment (JDrive will handle any ECONNRESET error)

