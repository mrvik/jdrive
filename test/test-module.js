/*eslint no-plusplus: 0*/
const fs=require("fs");
const path=require("path");
const babel=require("@babel/core");

function moduleTest(verbose){
    let dependencias=(function(){
        let deps;
        try{
            deps=JSON.parse(fs.readFileSync("package.json")).dependencies;
        }catch(e){
            console.error(e);
            throw new Error("Cannot load package.json");
        }
        return deps;
    })();
    Object.keys(dependencias).forEach(a=>{
        try{
            //eslint-disable-next-line global-require
            if(typeof require(a)=="undefined"){
                throw new Error("Node module "+a+" is not installed and is required");
            }else if(verbose)console.log("Module "+a+" is correctly installed");
        }catch(e){
            console.error(e);
            throw new Error("Some NodeJS modules are not installed: "+a);
        }
    });
    return true;
}
//eslint-disable-next-line max-lines-per-function
function codeTest(files,dirs,verbose){
    var tests={
        JS(pattern){
            var errors=[];
            var filesCount=0; //File and error count
            dirs.forEach(d=>{
                let list=fs.readdirSync(d).filter(a=>a.match(pattern));
                list.forEach(f=>{
                    let file=path.resolve([d, f].join("/"));
                    if(verbose)console.log(`Testing ${file}`);
                    try{
                        babel.transform(fs.readFileSync(file, "UTF-8"));
                        if(verbose)console.log(`${file} syntax correct`);
                    }catch(e){
                        console.error("Syntax incorrect on "+file, e.loc);
                        e.path=file;
                        console.log(e.message);
                        console.error(e.codeFrame);
                        errors.push(e);
                    }
                    filesCount++;
                });
            });
            return {filesCount,errors};
        },
        JSON(pattern){
            var filesCount=0;
            var errors=[];
            dirs.forEach(d=>{
                let list=fs.readdirSync(d).filter(a=>a.match(pattern));
                list.forEach(f=>{
                    let file=path.resolve([d,f].join("/"));
                    if(verbose)console.log(`Testing ${file}`);
                    try{
                        JSON.parse(fs.readFileSync(file, "UTF-8"));
                    }catch(e){
                        console.error("Incorrect JSON on "+file);
                        e.path=file;
                        console.log(e.message);
                        errors.push(e);
                    }
                    filesCount++;
                });
            });
            return {filesCount,errors};
        }
    };
    for(let i in files){
        if(!files.hasOwnProperty(i))continue;
        let res=tests[i](files[i]);
        let str=(`Tested ${res.filesCount} ${i} files. ${res.errors.length} error${res.errors.length==1 ? '' : 's'}`);
        if(res.errors.length>0)throw new Error(str);
        return console.log(str);
    }
}

module.exports={moduleTest,codeTest};
