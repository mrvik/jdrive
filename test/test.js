const {codeTest,moduleTest}=require('./test-module.js');

let verbose=process.env.NODE_ENV!='production';
let patterns={
    JS: /^.*\.js$/,
    JSON: /^.*\.json$/
};
let directories=[".",'lib','test'];
moduleTest(verbose);
codeTest(patterns,directories,verbose);
