# TODOs

### Very important, blocking todos

- Localfs
  - Test localfs events

- Drive
  - Write from localfs events


### Low importancy

- Status bar icon with the current state (maybe a separate package who reads the lockfile)
- Add README
- Add Privacy Policy
- Add icon (only visible from GitLab and the OAuth screen)
